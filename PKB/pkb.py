class PKB:
    def __init__(self):
        self.AST = None
        self.ProcTable = None
        self.VarTable = None
        self.StmtTable = None
        self.Calls = None
        self.Modifies = None
        self.Uses = None
        self.Parent = None
        self.Follows = None
        self.Next = None

    def __str__(self):
        return '\n'.join([str(element) for element in [
            self.ProcTable,
            self.VarTable,
            self.StmtTable,
            self.Calls,
            self.Modifies,
            self.Uses,
            self.Parent,
            self.Follows,
            self.Next
        ]])
