from PKB.SIMPLE.grammar_rules import *


def is_declared(var_name: str, variables: iter):
    return next((True for v in variables if v.name == var_name), False)


# używane do określania tego, który operator ma jaką wagę (grammar_rules.py)
def get_operator_precedence(operator: str):
    for index, group in enumerate(OPERATORS_PRECEDENCE):
        if operator in group:
            return index


# komparator między operatorami
def is_equal_or_greater_than(operator1: str, operator2: str):
    return get_operator_precedence(operator1) <= get_operator_precedence(operator2)


def stmt_lst_to_str(stmt_lst: iter):
    return '; '.join(stmt.get_ast_str() for stmt in stmt_lst)
