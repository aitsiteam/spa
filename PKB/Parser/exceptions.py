ERROR_FORMAT = 'SIMPLE Error while parsing {0}({1}):\n\tError: {3}\n\t\t-> Line {1}: {2}'

EXPECTED_TOKEN = "{} expected."
INVALID_NAME = 'Invalid name {}.'
RESTRICTED_KEYWORD = '{} is a restricted keyword.'
UNEXPECTED_END_OF_FILE = 'Unexpected end of file.'
PARENTHESES_MISMATCH = 'Parentheses mismatch in expression.'


class SIMPLEException(Exception):
    def __init__(self, msg: str):
        self.msg = msg

        super().__init__(msg)


class ParserException(SIMPLEException):
    def __init__(self, state, msg: str):
        super().__init__(ERROR_FORMAT.format(state.file_name, state.line_number, state.line, msg))


class TokenNotFoundException(SIMPLEException):
    def __init__(self, expected: str):
        super().__init__(EXPECTED_TOKEN.format(expected))


class InvalidNameException(SIMPLEException):
    def __init__(self, name: str):
        super().__init__(INVALID_NAME.format(name))


class RestrictedKeywordException(SIMPLEException):
    def __init__(self, keyword: str):
        super().__init__(RESTRICTED_KEYWORD.format(keyword))


class ParenthesesMismatchException(SIMPLEException):
    def __init__(self):
        super().__init__(PARENTHESES_MISMATCH)


class EndOfFileException(SIMPLEException):
    def __init__(self):
        super().__init__(UNEXPECTED_END_OF_FILE)
