import re
from PKB.Parser.common import *
from PKB.Parser.exceptions import *


# klasa odpowiedzi odnośnie wykonanego walidatora
class ValidationResult:
    def __init__(self, success: bool, exception=None, exception_args=None):
        self.success = success
        self.exception = exception
        self.exception_args = exception_args

    def __bool__(self):
        return self.success


# walidator sprawdzający, czy dany token == token'owi, który oczekujemy, czyli token1 == token2
def validate_match(token: str, target: str):
    return ValidationResult(token == target, TokenNotFoundException, target)


# walidator sprawdzający, czy token jest nazwą (do zmiennych, nazwy procedur)
def validate_is_name(token: str):
    # jeśli nazwa nie zgadza się z regex'em, który sprawdza, czy token jest nazwą, to False i odpowiedni wyjątek
    if not re.match(NAME_RE, token):
        return ValidationResult(False, InvalidNameException, token)

    # jeśli nazwa znajduje się w zbiorze słów kluczowych, to False - nazwa nie może być słowem kluczowym i wyjątek
    if token in KEYWORDS:
        return ValidationResult(False, RestrictedKeywordException, token)

    # jak wsio git, to git
    return ValidationResult(True)


# walidator sprawdzający, czy token jest liczbą (całkowitą - tylko takie w SIMPLE są)
def validate_is_integer(token: str):
    return ValidationResult(bool(re.match(INTEGER_RE, token)), Exception)


# walidator sprawdzający, czy token jest operatorem matematycznym
def is_operator(token: str):
    return ValidationResult(token in OPERATORS)


# walidator sprawdzający, czy token jest liczbą (stałą), zmienną lub otwierającym nawiasem
# czyli sprawdza to sytuację np. a = ?, bo teraz ? może być
# stałą: a = 1, zmienną: a = b, nawiasem '(': a = (...
def validate_is_const_or_variable_or_left_parenthesis(token: str):
    # jeśli jest zmienną lub stałą, to dalej oczekujemy, że będzie
    # operatorem, zamkniętym nawiasem ')' lub średnikiem ';', bo:
    # a = b ...:
    #   a = b
    #   a = b)      // może być tak, że wcześniej otwieraliśmy nawias ;)
    #   a = b;
    # analogicznie ze stałą (liczbą)
    if validate_is_name(token) or validate_is_integer(token):
        return [is_operator_or_right_parenthesis_or_semicolon]

    # jeśli jest otwierającym nawiasem '(', to wtedy mamy opcje np.:
    # a = ( ...
    #   a = (b ...
    #   a = (1 ...
    #   a = (( ...
    if token == LEFT_PARENTHESIS:
        return [validate_is_const_or_variable_or_left_parenthesis]

    # jak nie, to wyjątek, że expected liczba, zmienna lub '('
    return ValidationResult(False, TokenNotFoundException, 'const, var_name or (')


# walidator sprawdzający, czy token jest operatorem matematycznym, nawiasem zamykającym ')' lub średnikiem ';'
def is_operator_or_right_parenthesis_or_semicolon(token: str):
    # jeśli średnik, to spoko, koniec zabawy
    if token == SEMICOLON:
        return ValidationResult(True)

    # jeśli operator..., to mamy takie przypadki
    # a = 1 + ..., np.:
    #   a = 1 + 1
    #   a = 1 + b
    #   a = 1 + ( ...
    if is_operator(token):
        return [validate_is_const_or_variable_or_left_parenthesis]

    # jeśli nawias zamykający ')', to mamy takie przypadki
    # a = (...) ..., np.:
    #   a = (...) + ...
    #   a = ... (...))
    #   a = (...);
    if token == RIGHT_PARENTHESIS:
        return [is_operator_or_right_parenthesis_or_semicolon]

    # jak nie, to wyjątek, że expected operator ')' lub ';'
    return ValidationResult(False, TokenNotFoundException, 'operator, ) or ;')


# walidator zwracający listę walidatorów do sprawdzenia, czy jest poprawna składnia procedury
def get_procedure_validators():
    return [
        (validate_match, PROCEDURE_KEYWORD),    # czy pierwsze słówko 'procedure'
        validate_is_name,                       # czy następnie jest nazwa procedury
        (validate_match, LEFT_BRACE),           # czy następnie jest '{'
        get_stmt_validators,                    # musi być przynajmniej 1 stmt
        get_stmt_lst_validators                 # kolejne stmt'y (jeśli brak, to walidator sprawdza, czy jest '}')
    ]


# walidator sytuacyjny, zwracający listę walidatorów do sprawdzenia, czy jest poprawna składnia instrukcji (stmt)
# dla podanego tokena, jeśli token = procedure, to zwróci nam walidatory do sprawdzenia dalej, czy procedura jest
# poprawnie napisana, itd...
def get_stmt_validators(token: str):
    if token == CALL_KEYWORD:
        return get_call_stmt_validators()
    if token == WHILE_KEYWORD:
        return get_while_stmt_validators()
    if token == IF_KEYWORD:
        return get_if_stmt_validators()
    # jeśli jest to nazwa, to wiadomo, że to assign, więc walidatory do instrukcji assign
    if validate_is_name(token):
        return get_assign_stmt_validators()

    # jak nic nie zwróci, to znaczy, że w luja tniemy, bo ma być jakiś stmt, jeśli to nie jest żadne słowo kluczowe,
    # to musi to być assign, jeśli to nie jest assign, to kurna wyjątek, bo to nie jest żaden stmt, a ma być
    return ValidationResult(False, InvalidNameException, token)


# walidator sytuacyjny, zwracający listę walidatorów do sprawdzenia w przypadku, gdy może być stmt lub go może nie być
def get_stmt_lst_validators(token: str):
    # zakładamy, że mamy już jeden stmt w stmtLst, więc jeśli mamy zamknięcie scope'a (bloku instrukcji), to jest git,
    # więc jeśli token = }, to gitara, nie ma co tu sprawdzać dalej na tym etapie
    if token == RIGHT_BRACE:
        return ValidationResult(True)

    # jeśli jednak już coś tam wpisaliśmy, to to musi być instrukcja, więc sprawdzamy walidatorem stmt
    # po sprawdzeniu walidatorem stmt, może być kolejny stmt lub nie, stąd od razu dorzucamy ten sam walidator jak ten
    return get_stmt_validators(token) + [get_stmt_lst_validators]


# poniższe walidatory sprawdzają odpowiednie instrukcje
# zauważyć można, że brak jest tam sprawdzania słów kluczowych (np. czy jest słówko procedure, while, call, if)
# a to dlatego, że walidator get_stmt_validators to nam załatwia :)


# walidator zwracający listę walidatorów do sprawdzenia, czy jest poprawna składnia call
def get_call_stmt_validators():
    return [
        validate_is_name,                       # czy jest poprawna nazwa procedury
        (validate_match, SEMICOLON)             # czy jest średnik ';' na końcu
    ]


# walidator zwracający listę walidatorów do sprawdzenia, czy jest poprawna składnia while
def get_while_stmt_validators():
    return [
        validate_is_name,                       # czy jest poprawna nazwa zmiennej
        (validate_match, LEFT_BRACE),           # czy jest znak '{'
        get_stmt_validators,                    # czy jest przynajmniej 1 stmt
        get_stmt_lst_validators                 # kolejne stmt'y (jeśli brak, to walidator sprawdza, czy jest '}')
    ]


# walidator zwracający listę walidatorów do sprawdzenia, czy jest poprawna składnia if
def get_if_stmt_validators():
    return [
        validate_is_name,                       # czy jest poprawna nazwa zmiennej
        (validate_match, THEN_KEYWORD),         # czy jest słowo 'then'
        (validate_match, LEFT_BRACE),           # czy jest znak '{'
        get_stmt_validators,                    # czy jest przynajmniej 1 stmt
        get_stmt_lst_validators,                # kolejne stmt'y (jeśli brak, to walidator sprawdza, czy jest '}')
        (validate_match, ELSE_KEYWORD),         # czy jest słowo 'else'
        (validate_match, LEFT_BRACE),           # czy jest znak '{'
        get_stmt_validators,                    # czy jest przynajmniej 1 stmt
        get_stmt_lst_validators                 # kolejne stmt'y (jeśli brak, to walidator sprawdza, czy jest '}')
    ]


# walidator zwracający listę walidatorów do sprawdzenia, czy jest poprawna składnia instrukcji assign
def get_assign_stmt_validators():
    return [
        (validate_match, EQUALS_OPERATOR),                  # czy jest znak '='
        validate_is_const_or_variable_or_left_parenthesis   # i dalej magia związana z assign'em
    ]
