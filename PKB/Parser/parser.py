import datetime

from PKB.API.calls_api import CallsAPI
from PKB.API.follows_api import FollowsAPI
from PKB.API.next_api import NextAPI
from PKB.API.modifies_api import ModifiesAPI
from PKB.API.parent_api import ParentAPI
from PKB.API.proc_table_api import ProcTableAPI
from PKB.API.stmt_table_api import StmtTableAPI
from PKB.API.uses_api import UsesAPI
from PKB.API.var_table_api import VarTableAPI
from PKB.pkb import PKB
from PKB.Parser.validators import *
from PKB.AST.nodes import *


PROGRAM_PARSED_SUCCESSFULLY = 'Program parsed successfully in {} s.'


class ParserState:
    def __init__(self, file_name: str):
        self.file_name = file_name
        self.line = None
        self.line_number = 0
        self.prog_line = 0
        self.actual_prog_line = 0
        self.node = None
        self.node_data = []
        self.scopes = []
        self.tokens = []
        self.validators = get_procedure_validators()
        self.called_procedures = {}
        self.program = ProgramNode()
        self.ProcTable = ProcTableAPI()
        self.VarTable = VarTableAPI()
        self.StmtTable = StmtTableAPI()
        self.CallsTable = CallsAPI()
        self.ParentTable = ParentAPI()
        self.FollowsTable = FollowsAPI()
        self.NextTable = NextAPI()
        self.started_time = datetime.datetime.now()
        self.opened_stmt = []
        self.actual_content = []
        self.whiles = []
        self.whiles_to_bind = []
        self.ifs = []
        self.thens = dict()
        self.elses = dict()
        self.else_just_closed = False
        self.while_just_closed = False
        self.previous_was_else = False
        self.if_to_bind = None

    def print_ast(self):
        print('AST: ' + ', '.join(str(scope) for scope in self.scopes))

    def next_line(self, line: str, tokens: iter):
        self.line = line
        self.line_number += 1
        self.tokens = tokens

        if "procedure " in line or "else " in line:
            self.prog_line = None
        elif "call" in line or "while" in line or "if" in line or "=" in line:
            self.actual_prog_line += 1
            self.prog_line = self.actual_prog_line
            self.NextTable.add_key(self.prog_line)
        else:
            self.prog_line = None

        self.create_cfg(line)

    def get_current_scope(self):
        return self.scopes[-1] if len(self.scopes) > 0 else None

    def close_current_scope(self):
        if len(self.scopes) == 0:
            raise Exception('Unclosed scope?')

        self.scopes.pop(-1)

    def get_stmts_set(self, with_current: bool):
        stmts = {scope.prog_line for scope in self.scopes if isinstance(scope, (WhileNode, IfNode))}

        if with_current:
            if self.prog_line is not None:
                stmts.add(self.prog_line - 1)

        return stmts

    def close_stmt_cfg(self, bracket_quantity: int):
        for _ in range(bracket_quantity):
            if len(self.opened_stmt) > 0 and self.opened_stmt[-1] in self.ifs:
                # close if
                for index, prog_line in enumerate(self.actual_content):
                    if index < len(self.actual_content) - 1:
                        self.NextTable.add_next(prog_line, self.actual_content[index + 1])
                self.actual_content.clear()
                if self.else_just_closed:
                    then = self.thens.get(self.ifs[-1])[-1]
                    self.thens.pop(self.ifs[-1])
                    for t in self.thens.get(then):
                        self.thens.setdefault(self.ifs[-1], []).append(t)
                    for e in self.elses.get(then):
                        self.thens.setdefault(self.ifs[-1], []).append(e)
                if self.while_just_closed:
                    self.whiles_to_bind.pop()
                self.while_just_closed = False
                self.else_just_closed = False
            if len(self.opened_stmt) > 0 and self.opened_stmt[-1] not in self.ifs:
                if self.opened_stmt[-1] is not None:
                    # close while
                    if self.while_just_closed:
                        if self.whiles[-1] not in self.NextTable.get_record(self.whiles_to_bind[-1]):
                            self.NextTable.add_next(self.whiles_to_bind[-1], self.whiles[-1])
                            self.whiles_to_bind.pop()
                    else:
                        for index, prog_line in enumerate(self.actual_content):
                            if index < len(self.actual_content) - 1:
                                self.NextTable.add_next(prog_line, self.actual_content[index + 1])
                        if self.else_just_closed:
                            for t in self.thens.get(self.if_to_bind):
                                if self.prog_line not in self.NextTable.get_record(t):
                                    self.NextTable.add_next(t, self.whiles[-1])
                            for e in self.elses.get(self.if_to_bind):
                                if self.prog_line not in self.NextTable.get_record(e):
                                    self.NextTable.add_next(e, self.whiles[-1])
                        elif self.whiles[-1] not in self.NextTable.get_record(self.actual_prog_line):
                            self.NextTable.add_next(self.actual_prog_line, self.whiles[-1])
                    self.opened_stmt.pop()
                    self.whiles.pop()
                    self.actual_content.clear()
                    self.while_just_closed = True
                    self.else_just_closed = False
                else:
                    # close else
                    for index, prog_line in enumerate(self.actual_content):
                        if index < len(self.actual_content) - 1:
                            self.NextTable.add_next(prog_line, self.actual_content[index + 1])
                    self.opened_stmt.pop()  # remove else
                    self.opened_stmt.pop()  # remove if
                    self.actual_content.clear()
                    self.if_to_bind = self.ifs[-1]
                    self.ifs.pop()
                    if self.else_just_closed:
                        els = self.elses.get(self.if_to_bind)[-1]
                        self.elses.pop(self.if_to_bind)
                        for t in self.thens.get(els):
                            self.elses.setdefault(self.if_to_bind, []).append(t)
                        for e in self.elses.get(els):
                            self.elses.setdefault(self.if_to_bind, []).append(e)
                    if self.while_just_closed:
                        self.whiles_to_bind.pop()
                    self.else_just_closed = True
                    self.while_just_closed = False
            elif len(self.opened_stmt) == 0:
                # close procedure
                for index, prog_line in enumerate(self.actual_content):
                    if index < len(self.actual_content) - 1:
                        self.NextTable.add_next(prog_line, self.actual_content[index + 1])
                self.opened_stmt = []
                self.actual_content = []
                self.whiles = []
                self.whiles_to_bind = []
                self.ifs = []
                self.thens = dict()
                self.elses = dict()
                self.else_just_closed = False
                self.while_just_closed = False
                self.previous_was_else = False

    def create_cfg(self, line):
        try:
            starts_with_cloing_bracket = False
            keywords = ["procedure", "while", "if", "else", "call", "="]
            if line.lstrip().startswith("}") and any(ext in line for ext in keywords):
                starts_with_cloing_bracket = True
                bracket_quantity = line.count("}")
                self.close_stmt_cfg(bracket_quantity)
            if " procedure " in line and "=" not in line:
                self.opened_stmt.clear()
                self.actual_content.clear()
            elif " while " in line and "=" not in line:
                if self.while_just_closed:
                    self.NextTable.add_next(self.whiles_to_bind[-1], self.prog_line)
                    self.whiles_to_bind.pop()
                self.whiles.append(self.prog_line)
                self.whiles_to_bind.append(self.prog_line)
                if self.opened_stmt:
                    if self.opened_stmt[-1] in self.whiles or self.opened_stmt[-1] in self.ifs:
                        if not self.NextTable.get_record(self.opened_stmt[-1]):
                            self.NextTable.add_next(self.opened_stmt[-1], self.prog_line)
                        if self.opened_stmt[-1] in self.ifs:
                            if self.thens.get(self.ifs[-1]) is not None:
                                self.thens.pop(self.ifs[-1])
                            self.thens.setdefault(self.ifs[-1], []).append(self.prog_line)
                    elif self.opened_stmt[-1] is None:
                        if self.elses.get(self.ifs[-1]) is not None:
                            self.elses.pop(self.ifs[-1])
                        self.elses.setdefault(self.ifs[-1], []).append(self.prog_line)
                    if self.previous_was_else:
                        self.NextTable.add_next(self.ifs[-1], self.prog_line)
                self.actual_content.append(self.prog_line)
                for index, prog_line in enumerate(self.actual_content):
                    if index < len(self.actual_content) - 1:
                        self.NextTable.add_next(prog_line, self.actual_content[index + 1])
                if self.else_just_closed:
                    for t in self.thens.get(self.if_to_bind):
                        if self.prog_line not in self.NextTable.get_record(t):
                            self.NextTable.add_next(t, self.prog_line)
                    for e in self.elses.get(self.if_to_bind):
                        if self.prog_line not in self.NextTable.get_record(e):
                            self.NextTable.add_next(e, self.prog_line)
                self.actual_content.clear()
                self.opened_stmt.append(self.prog_line)
                self.previous_was_else = False
                self.else_just_closed = False
                self.while_just_closed = False
            elif " if " in line and "=" not in line:
                self.ifs.append(self.prog_line)
                self.actual_content.append(self.prog_line)
                if self.while_just_closed:
                    self.NextTable.add_next(self.whiles_to_bind[-1], self.prog_line)
                    self.whiles_to_bind.pop()
                if self.opened_stmt:
                    if self.opened_stmt[-1] in self.whiles or self.opened_stmt[-1] in self.ifs:
                        if not self.NextTable.get_record(self.opened_stmt[-1]):
                            self.NextTable.add_next(self.opened_stmt[-1], self.prog_line)
                        if self.opened_stmt[-1] in self.ifs:
                            if self.thens.get(self.ifs[-2]) is not None:
                                self.thens.pop(self.ifs[-2])
                            self.thens.setdefault(self.ifs[-2], []).append(self.prog_line)
                    elif self.opened_stmt[-1] is None:
                        if self.elses.get(self.ifs[-2]) is not None:
                            self.elses.pop(self.ifs[-2])
                        self.elses.setdefault(self.ifs[-2], []).append(self.prog_line)
                    if self.previous_was_else:
                        self.NextTable.add_next(self.ifs[-2], self.prog_line)
                for index, prog_line in enumerate(self.actual_content):
                    if index < len(self.actual_content) - 1:
                        self.NextTable.add_next(prog_line, self.actual_content[index + 1])
                if self.else_just_closed:
                    for t in self.thens.get(self.if_to_bind):
                        if self.prog_line not in self.NextTable.get_record(t):
                            self.NextTable.add_next(t, self.prog_line)
                    for e in self.elses.get(self.if_to_bind):
                        if self.prog_line not in self.NextTable.get_record(e):
                            self.NextTable.add_next(e, self.prog_line)
                self.actual_content.clear()
                self.opened_stmt.append(self.prog_line)
                self.previous_was_else = False
                self.else_just_closed = False
                self.while_just_closed = False
            elif " else " in line and "=" not in line:
                self.opened_stmt.append(self.prog_line)
                self.previous_was_else = True
                self.else_just_closed = False
                self.while_just_closed = False
            elif " call " in line or "=" in line:
                self.actual_content.append(self.prog_line)
                if self.opened_stmt:
                    if self.opened_stmt[-1] in self.ifs:
                        if self.thens.get(self.ifs[-1]) is not None:
                            self.thens.pop(self.ifs[-1])
                        self.thens.setdefault(self.ifs[-1], []).append(self.prog_line)
                        if not self.NextTable.get_record(self.opened_stmt[-1]):
                            self.NextTable.add_next(self.opened_stmt[-1], self.prog_line)
                    elif self.opened_stmt[-1] is None:
                        if self.elses.get(self.ifs[-1]) is not None:
                            self.elses.pop(self.ifs[-1])
                        self.elses.setdefault(self.ifs[-1], []).append(self.prog_line)
                        if self.previous_was_else:
                            self.NextTable.add_next(self.ifs[-1], self.prog_line)
                    elif self.opened_stmt[-1] in self.whiles:
                        if not self.NextTable.get_record(self.opened_stmt[-1]):
                            self.NextTable.add_next(self.opened_stmt[-1], self.prog_line)
                if self.while_just_closed:
                    self.NextTable.add_next(self.whiles_to_bind[-1], self.prog_line)
                    self.whiles_to_bind.pop()
                if self.else_just_closed:
                    for t in self.thens.get(self.if_to_bind):
                        if self.prog_line not in self.NextTable.get_record(t):
                            self.NextTable.add_next(t, self.prog_line)
                    for e in self.elses.get(self.if_to_bind):
                        if self.prog_line not in self.NextTable.get_record(e):
                            self.NextTable.add_next(e, self.prog_line)
                self.previous_was_else = False
                self.else_just_closed = False
                self.while_just_closed = False
            bracket_quantity = line.count("}")
            if starts_with_cloing_bracket:
                bracket_quantity -= 1
            self.close_stmt_cfg(bracket_quantity)
        except:
            return None


def parse(file_name: str) -> PKB:
    # init ParserState
    state = ParserState(file_name)

    try:
        print('Parsing file ' + file_name)

        with open(file_name) as file:
            for line in file:
                tokens = _get_tokens(line)
                state.next_line(line, tokens)
                _next(state)
        _end(state)

        pkb = PKB()
        pkb.ProcTable = state.ProcTable
        pkb.VarTable = state.VarTable
        pkb.StmtTable = state.StmtTable
        pkb.Calls = state.CallsTable
        pkb.Modifies = ModifiesAPI(pkb.ProcTable, pkb.VarTable)
        pkb.Uses = UsesAPI(pkb.ProcTable, pkb.VarTable)
        pkb.Parent = state.ParentTable
        pkb.Follows = state.FollowsTable
        pkb.Next = state.NextTable

        return pkb
    except SIMPLEException as e:
        raise ParserException(state, str(e))


def _next(state: ParserState):
    parentheses = 0

    for token in state.tokens:
        if len(state.validators) == 0:
            state.validators = get_procedure_validators()

        validator = state.validators.pop(0)
        function = validator[0] if isinstance(validator, tuple) else validator

        if isinstance(validator, tuple):
            args = validator[1]
            result = function(*(token, args))
        else:
            result = function(token)

        if isinstance(result, list):
            state.validators = result + state.validators
        elif not result:
            raise result.exception(result.exception_args)

        scope = state.get_current_scope()

        if token == SEMICOLON:
            if isinstance(state.node, AssignNode):
                if parentheses != 0:
                    raise ParenthesesMismatchException
                expr_node = _parse_expression(state.node_data)
                state.node.expr = expr_node

                _add_stmt(scope, state.node, state.FollowsTable)

                if isinstance(scope, (WhileNode, IfNode)):
                    upper_parents = state.get_stmts_set(False)
                    state.ParentTable.add_parents(scope.prog_line, upper_parents, state.prog_line - 1)
                state.node_data.clear()
                state.node = None
        elif token == RIGHT_BRACE:
            if isinstance(scope, IfNode) and scope.if_then:
                scope.if_then = False
            else:
                state.close_current_scope()

                if isinstance(scope, ProcedureNode):
                    state.program.procedures.append(scope)
                elif isinstance(scope, (WhileNode, IfNode)):
                    new_scope = state.get_current_scope()

                    _add_stmt(new_scope, scope, state.FollowsTable)

            state.node = None
        elif not state.node:
            state.node = _create_node(token, state.prog_line)
            state.StmtTable.add_stmt(state.node)

            if isinstance(state.node, (CallNode, WhileNode, IfNode, AssignNode)):
                state.prog_line += 1

            if isinstance(state.node, AssignNode):
                state.node.name = token
                state.ProcTable.add_modifies(state.scopes[0].name, token)
                state.VarTable.add_record(token)
                state.VarTable.add_modified_in(token, state.get_stmts_set(True))
        elif validate_is_name(token).success and not state.node.name:
            state.node.name = token

            if isinstance(state.node, CallNode):
                _add_stmt(scope, state.node, state.FollowsTable)
                current_procedure = state.scopes[0]
                calls_info = {
                    state.prog_line - 1: {
                        'called': token,
                        'upper_stmts': state.get_stmts_set(False)
                    }
                }
                state.called_procedures.update(calls_info)
                state.CallsTable.add_calls(current_procedure.name, token)
            else:
                if isinstance(state.node, ProcedureNode):
                    state.ProcTable.add_record(state.node)
                else:
                    state.ProcTable.add_uses(state.scopes[0].name, token)
                    state.VarTable.add_record(token)
                    state.VarTable.add_used_in(token, state.get_stmts_set(True))

                state.scopes.append(state.node)

            if isinstance(scope, (WhileNode, IfNode)):
                upper_parents = state.get_stmts_set(False)

                if isinstance(state.node, (WhileNode, IfNode)):
                    upper_parents.remove(state.prog_line - 1)

                state.ParentTable.add_parents(scope.prog_line, upper_parents, state.prog_line - 1)

            state.node = None
        elif isinstance(state.node, AssignNode):
            if validate_is_name(token).success or validate_is_integer(token).success or token in MATH_EXPR_TOKENS:
                if state.node.name and validate_is_name(token).success:
                    state.VarTable.add_record(token)
                    state.ProcTable.add_uses(state.scopes[0].name, token)
                    state.VarTable.add_used_in(token, state.get_stmts_set(True))

                state.node_data.append(token)

                if validate_is_integer(token).success:
                    state.StmtTable.add_constant(token)

                if token == LEFT_PARENTHESIS:
                    parentheses += 1
                elif token == RIGHT_PARENTHESIS:
                    parentheses -= 1


def _end(state: ParserState):
    if len(state.validators) > 0:
        function = state.validators[0][0] if isinstance(state.validators[0], tuple) else state.validators[0]

        if function is validate_match:
            expected = state.validators[0][1]

            raise TokenNotFoundException(expected)
        if function is validate_is_name:
            raise TokenNotFoundException('name')

        raise EndOfFileException

    for procedure in state.ProcTable.get_all_procedures():
        for called_procedure in state.CallsTable.get_called_from_t(procedure):
            from_procedure = state.ProcTable.get_record(called_procedure)

            state.ProcTable.add_modifies_range(procedure, from_procedure.modifies)
            state.ProcTable.add_uses_range(procedure, from_procedure.uses)

    for variable in state.VarTable.get_all_variables():
        for stmt, record in state.called_procedures.items():
            called = record['called']
            upper_stmts = record['upper_stmts']
            stmts = {stmt}
            stmts.update(upper_stmts)

            if variable in state.ProcTable.get_record(called).modifies:
                state.VarTable.add_modified_in(variable, stmts)

            if variable in state.ProcTable.get_record(called).uses:
                state.VarTable.add_used_in(variable, stmts)

    duration = datetime.datetime.now() - state.started_time
    duration_display = str(duration.seconds) + '.' + str(duration.microseconds)

    print('\033[2;30;42m' + PROGRAM_PARSED_SUCCESSFULLY.format(duration_display))
    print('\033[91m')
    print(state.program)
    # _convert_ast_to_readable_form(state.program)


def _get_tokens(line: str):
    for token in TOKENS:
        line = line.replace(token, ' ' + token + ' ')

    return line.split()


def _create_node(token: str, prog_line):
    if token == PROCEDURE_KEYWORD:
        return ProcedureNode()

    if token == CALL_KEYWORD:
        return CallNode(prog_line)

    if token == WHILE_KEYWORD:
        return WhileNode(prog_line)

    if token == IF_KEYWORD:
        return IfNode(prog_line)

    if validate_is_name(token).success:
        return AssignNode(prog_line)

    return None


def _parse_expression(tokens: iter):
    tokens.append(';')
    operands_stack = []
    operators_stack = []

    for token in tokens:
        if token == SEMICOLON:
            while len(operators_stack) > 0:
                operator = operators_stack.pop(-1)
                left = operands_stack.pop(-2)
                right = operands_stack.pop(-1)
                node = OperatorNode(operator, left, right)

                operands_stack.append(node)

            return operands_stack[0]
        elif validate_is_integer(token).success or validate_is_name(token).success:
            operands_stack.append(token)
        elif token == LEFT_PARENTHESIS:
            operators_stack.append(token)
        elif token == RIGHT_PARENTHESIS:
            while operators_stack[-1] != LEFT_PARENTHESIS:
                operator = operators_stack.pop(-1)
                left = operands_stack.pop(-2)
                right = operands_stack.pop(-1)
                node = OperatorNode(operator, left, right)

                operands_stack.append(node)
            operators_stack.pop(-1)
        else:
            while len(operators_stack) > 0 and is_equal_or_greater_than(operators_stack[-1], token) and operators_stack[-1] != LEFT_PARENTHESIS:
                operator = operators_stack.pop(-1)
                left = operands_stack.pop(-2)
                right = operands_stack.pop(-1)
                node = OperatorNode(operator, left, right)
                operands_stack.append(node)
            operators_stack.append(token)


def _add_stmt(scope, node, follows_table: FollowsAPI):
    if isinstance(scope, IfNode):
        if scope.if_then:
            last_stmt = scope.then_stmt_lst[-1].prog_line if len(scope.then_stmt_lst) > 0 else None
            scope.add_then_stmt(node)
        else:
            last_stmt = scope.else_stmt_lst[-1].prog_line if len(scope.else_stmt_lst) > 0 else None
            scope.add_else_stmt(node)

        if last_stmt:
            follows_table.add_follows(last_stmt, node.prog_line)
    else:
        last_stmt = scope.stmt_lst[-1].prog_line if len(scope.stmt_lst) > 0 else None

        if last_stmt:
            follows_table.add_follows(last_stmt, node.prog_line)

        scope.add_stmt(node)


def _test(procedure: str, calls_table: CallsAPI):
    for called_procedure in calls_table.get_called_from(procedure):
        pass


def _convert_ast_to_readable_form(program: ProgramNode):
    print('Program')

    for procedure in program.procedures:
        print('procedure(' + procedure.name + '):')
        _print_stmt_lst_in_readable_form(procedure.stmt_lst, 1)
        print()


def _print_stmt_lst_in_readable_form(stmt_lst: iter, level: int):
    tabs = ''

    for i in range(level):
        tabs += '\t'

    for stmt in stmt_lst:
        stmt_prog_line_text = str(stmt.prog_line) + '.' + tabs

        if isinstance(stmt, WhileNode):
            print(stmt_prog_line_text + 'while(' + stmt.name + '):')
            _print_stmt_lst_in_readable_form(stmt.stmt_lst, level + 1)
        elif isinstance(stmt, IfNode):
            print(stmt_prog_line_text + 'if(' + stmt.name + ') then:')
            _print_stmt_lst_in_readable_form(stmt.then_stmt_lst, level + 1)
            print(tabs + 'else:')
            _print_stmt_lst_in_readable_form(stmt.else_stmt_lst, level + 1)
        else:
            print(stmt_prog_line_text + str(stmt))
