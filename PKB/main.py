import os
import traceback

from PKB.Parser.parser import parse
from PKB.Parser.exceptions import ParserException


def main():
    dir_name = os.path.dirname(__file__)
    program_number = 'source'
    file_name = os.path.join(dir_name, 'Parser/SIMPLE Samples/program' + str(program_number) + '.txt')

    try:
        pkb = parse(file_name)
        print(pkb)
    except ParserException as e:
        tb = traceback.format_exc()
        print('\033[7;31;40mSIMPLE Parser exception!')
        print('\033[91m' + str(e) + '\n' + tb)
    except Exception:
        tb = traceback.format_exc()
        print('\033[7;31;40mMajor exception!')
        print('\033[91m' + tb)


if __name__ == '__main__':
    main()
