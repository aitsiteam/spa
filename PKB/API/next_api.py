class NextAPI:
    def __init__(self):
        self._NextTable = dict()

    def __str__(self):
        return 'NextTable: ' + str(self._NextTable)

    # adds Next relation to NextTable
    def add_next(self, prog_line1: int, prog_line2: int):
        self._NextTable.setdefault(prog_line1, []).append(prog_line2)

    def add_key(self, prog_line1: int):
        self._NextTable.setdefault(prog_line1, [])

    def get_record(self, prog_line: int):
        return self._NextTable.get(prog_line)

    # Next(s, _)
    # returns prog_line
    # get stmt which is directly after by stmt s
    def get_stmts_after(self, prog_line: int) -> tuple:
        if prog_line > len(self._NextTable):
            return ()
        result = [stmt for stmt in self.get_record(prog_line)]
        return tuple(result)

    # Next*(s, _)
    # returns list of prog_lines
    # get all stmts which are directly or indirectly after stmt s
    def get_stmts_after_t(self, prog_line: int) -> tuple:
        if prog_line > len(self._NextTable):
            return ()
        stmts = ()
        for follower, stmt_lst in self._NextTable.items():
            for stmt in stmt_lst:
                if self.is_next_t(prog_line, stmt):
                    if stmt not in stmts:
                        stmts = stmts + (stmt, )
        return stmts

    # Next(_, s)
    # returns prog_line
    # get stmt which is directly before stmt s
    def get_stmts_before(self, prog_line: int) -> tuple:
        if prog_line > len(self._NextTable):
            return ()
        result = [stmt for stmt, followed in self._NextTable.items() if prog_line in followed]
        return tuple(result)

    # Next*(_, s)
    # returns prog_line
    # get all stmts which are directly or indirectly before stmt s
    def get_stmts_before_t(self, prog_line: int) -> tuple:
        if prog_line > len(self._NextTable):
            return ()
        stmts = ()
        for follower, stmt_lst in self._NextTable.items():
            if self.is_next_t(follower, prog_line):
                if follower not in stmts:
                    stmts = stmts + (follower, )
        return stmts

    # Next* (n1, n2) -> select n2
    # returns prog_line
    # get all stmts which have any follower
    def get_stmts_which_have_follower(self) -> tuple:
        stmts = ()
        joined_list = []
        for a in self._NextTable.values():
            joined_list += a
        for key, stmt_lst in self._NextTable.items():
            if key in joined_list:
                if key not in stmts:
                    stmts = stmts + (key, )
        return stmts

    # Next* (n1, n2) -> select n1
    # returns prog_line
    # get all stmts which have any successor
    def get_stmts_which_have_successor(self) -> tuple:
        stmts = ()
        for key, stmt_lst in self._NextTable.items():
            if stmt_lst:
                if key not in stmts:
                    stmts = stmts + (key, )
        return stmts

    # Next(s1, s2)
    # returns True/False
    # if stmt s1 directly follows stmt s2
    def is_next(self, prog_line1: int, prog_line2: int) -> bool:
        if prog_line1 > len(self._NextTable) or prog_line2 > len(self._NextTable):
            return False
        return prog_line2 in self.get_record(prog_line1)

    # Next*(s1, s2)
    # returns True/False
    # if stmt s1 directly or indirectly follows stmt s2
    def is_next_t(self, prog_line1: int, prog_line2: int) -> bool:

        def remove_item():
            for prog_line, values in all_children.items():
                if last_key in values:
                    values.remove(last_key)
                    if len(values) == 0:
                        values = []
                    all_children[prog_line] = []
                    all_children.setdefault(prog_line, values)
            removed.append(last_key)
            if all_children.get(last_key) is not None:
                all_children.pop(last_key)

        if prog_line1 > len(self._NextTable) or prog_line2 > len(self._NextTable):
            return False
        try:
            removed = []
            stmts = self.get_record(prog_line1)
            all_children = dict()

            if prog_line2 in stmts:
                return True
            for child in stmts:
                all_children.setdefault(prog_line1, []).append(child)
            while all_children:
                joined_list = []
                for a in all_children.values():
                    joined_list += a
                if joined_list:
                    max_value = max(joined_list)
                    joined_list = []
                    for ind, val in all_children.items():
                        if max_value in val:
                            joined_list.append(ind)
                    last_key = max(joined_list)
                    if max_value <= max(all_children.keys()):
                        last_key = max(all_children.keys())
                else:
                    last_key = max(all_children.keys())
                child_line = all_children.get(last_key)
                if child_line:
                    for child in child_line:
                        for c in self.get_record(child):
                            if c == prog_line2:
                                return True
                            if c not in removed:
                                if all_children.get(child) is not None:
                                    if c not in all_children.get(child):
                                        all_children.setdefault(child, []).append(c)
                                    else:
                                        if len(all_children.get(last_key)) == 1:
                                            if all_children.get(last_key)[-1] < last_key:
                                                remove_item()
                                        break
                                else:
                                    all_children.setdefault(child, []).append(c)
                            else:
                                if len(self.get_record(child)) <= 1:
                                    last_key = child_line[-1]
                                    remove_item()
                                    break
                                if len(all_children.get(last_key)) == 1:
                                    if all_children.get(last_key)[-1] < last_key:
                                        remove_item()
                        if not self.get_record(child):
                            remove_item()
                else:
                    remove_item()
            return False
        except KeyError:
            return False
