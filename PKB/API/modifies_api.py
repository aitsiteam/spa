from PKB.API.proc_table_api import ProcTableAPI
from PKB.API.var_table_api import VarTableAPI


class ModifiesAPI:
    def __init__(self, proc_table: ProcTableAPI, var_table: VarTableAPI):
        self._ProcTable = proc_table
        self._VarTable = var_table

    def __str__(self):
        return 'ModifiesTable: { ' + str(self._ProcTable) + ', ' + str(self._VarTable) + ' }'

    # Modifies(p, _) [for procedures]
    # returns tuple of variables
    # gets all variables which are modified in procedure with proc_name = procedure
    def get_modifies_procedure(self, procedure: str) -> tuple:
        return self._ProcTable.get_modifies(procedure)

    # Modifies(s, _) [for stmt#]
    # returns tuple of variables
    # gets all variables which are modified in line = prog_line (prog_line = stmt#)
    def get_modifies_stmt(self, prog_line: int) -> tuple:
        return self._VarTable.get_modifies(prog_line)

    # Modifies(_, v) [for procedures]
    # returns tuple of procedures
    # gets all procedures which modifies variable with var_name = variable
    def get_modified_in_procedure(self, variable: str) -> tuple:
        return self._ProcTable.get_modified_in(variable)

    # Modifies(_, v) [for stmt#]
    # returns tuple of prog_lines
    # gets all prog_lines where variable with var_name = variable are modified
    def get_modified_in_stmt(self, variable: str) -> tuple:
        return self._VarTable.get_modified_in(variable)

    # Modifies(p, v) [for procedures]
    # returns True/False
    # if procedure with proc_name = procedure modifies variable with var_name = variable
    def is_modifies_procedure(self, procedure: str, variable: str) -> bool:
        return self._ProcTable.is_modifies(procedure, variable)

    # Modifies(s, v) [for stmt#]
    # returns True/False
    # if stmt with prog_line = prog_line modifies variable with var_name = variable
    def is_modifies_stmt(self, prog_line: int, variable: str) -> bool:
        return self._VarTable.is_modifies(prog_line, variable)
