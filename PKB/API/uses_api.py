from PKB.API.proc_table_api import ProcTableAPI
from PKB.API.var_table_api import VarTableAPI


class UsesAPI:
    def __init__(self, proc_table: ProcTableAPI, var_table: VarTableAPI):
        self._ProcTable = proc_table
        self._VarTable = var_table

    def __str__(self):
        return 'UsesTable: { ' + str(self._ProcTable) + ', ' + str(self._VarTable) + ' }'

    # Uses(p, _) [for procedures]
    # returns tuple of variables
    # gets all variables which are used in procedure with proc_name = procedure
    def get_uses_procedure(self, procedure: str) -> tuple:
        return self._ProcTable.get_uses(procedure)

    # Uses(s, _) [for stmt#]
    # returns tuple of variables
    # gets all variables which are used in line = prog_line (prog_line = stmt#)
    def get_uses_stmt(self, prog_line: int) -> tuple:
        return self._VarTable.get_uses(prog_line)

    # Uses(_, v) [for procedures]
    # returns tuple of procedures
    # gets all procedures which uses variable with var_name = variable
    def get_used_in_procedure(self, variable: str) -> tuple:
        return self._ProcTable.get_used_in(variable)

    # Uses(_, v) [for stmt#]
    # returns tuple of prog_lines
    # gets all prog_lines where variable with var_name = variable are used
    def get_used_in_stmt(self, variable: str) -> tuple:
        return self._VarTable.get_used_in(variable)

    # Uses(p, v) [for procedures]
    # returns True/False
    # if procedure with proc_name = procedure uses variable with var_name = variable
    def is_uses_procedure(self, procedure: str, variable: str) -> bool:
        return self._ProcTable.is_uses(procedure, variable)

    # Uses(s, v) [for stmt#]
    # returns True/False
    # if stmt with prog_line = prog_line uses variable with var_name = variable
    def is_uses_stmt(self, prog_line: int, variable: str) -> bool:
        return self._VarTable.is_uses(prog_line, variable)
