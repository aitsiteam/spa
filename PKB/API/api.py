from PKB.pkb import PKB


class API:
    def __init__(self, pkb: PKB):
        self._PKB = pkb

    # Poniższe metody powinny być wydzielone do odpowiednich plików od API
    # TODO NA RAZIE NA SZTYWNO MOCK - TO DO -
    def get_procedure_list(self):
        return list(['A', 'B', 'C', 'D', 'E', 'F'])

    # TODO
    def get_statement_list(self):
        return list([1, 3, 4, 5])

    def get_variable_list(self):
        return list(['a', 'd', 'e'])

    # TODO
    def get_prog_line_list(self):
        return list([1, 3, 4, 5])

    # TODO
    def is_next(self, prog_line1: int, prog_line2: int) -> bool:
        return True

    # TODO
    def is_next_t(self, prog_line1: int, prog_line2: int) -> bool:
        return True

    # TODO
    def is_parent(self, stmt1: int, stmt2: int) -> bool:
        return True

    # TODO
    def is_parent_t(self, stmt1: int, stmt2: int) -> bool:
        return True

    # TODO
    def is_follows(self, stmt1: int, stmt2: int) -> bool:
        return True

    # TODO
    def is_follows_t(self, stmt1: int, stmt2: int) -> bool:
        return True

    # TODO
    def is_modifies_stmt(self, stmt: int, variable: str) -> bool:
        return True

    # TODO
    def is_modifies_procedure(self, procedure: str, variable: str) -> bool:
        return True

    # TODO
    def is_uses_stmt(self, stmt: int, variable: str) -> bool:
        return True

    # TODO
    def is_uses_procedure(self, procedure: str, variable: str) -> bool:
        return True
