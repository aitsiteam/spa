from PKB.AST.nodes import StmtNode, CallNode, WhileNode, IfNode, AssignNode


class StmtTableAPI:
    def __init__(self):
        self._call_stmts = []
        self._while_stmts = []
        self._if_stmts = []
        self._assign_stmts = []
        self._constants = set()

    def __str__(self):
        return 'StmtTable: calls: {} | whiles: {} | ifs: {} | assigns: {} | constants: {}'.format(
            self._call_stmts,
            self._while_stmts,
            self._if_stmts,
            self._assign_stmts,
            self._constants
        )

    def add_stmt(self, stmt: StmtNode):
        if not isinstance(stmt, StmtNode):
            return

        prog_line = stmt.prog_line

        if isinstance(stmt, CallNode):
            new_record = {
                'proc_name': stmt.name,
                'prog_line': prog_line
            }

            self._call_stmts.append(prog_line)
        elif isinstance(stmt, WhileNode):
            self._while_stmts.append(prog_line)
        elif isinstance(stmt, IfNode):
            self._if_stmts.append(prog_line)
        elif isinstance(stmt, AssignNode):
            self._assign_stmts.append(prog_line)

    def add_constant(self, value: int):
        self._constants.add(int(value))

    # returns tuple of prog_lines
    # gets all call stmts from program
    def get_all_calls(self) -> tuple:
        return tuple(self._call_stmts)

    # returns tuple of prog_lines
    # gets all while stmts from program
    def get_all_whiles(self) -> tuple:
        return tuple(self._while_stmts)

    # returns tuple of prog_lines
    # gets all if stmts from program
    def get_all_ifs(self) -> tuple:
        return tuple(self._if_stmts)

    # returns tuple of prog_lines
    # gets all assign stmts from program
    def get_all_assign(self) -> tuple:
        return tuple(self._assign_stmts)

    # returns tuple of prog_lines
    # gets all stmts from program
    def get_all_stmts(self) -> tuple:
        return tuple(self._call_stmts + self._while_stmts + self._if_stmts + self._assign_stmts)

    # returns tuple of constants values
    # gets all constants from program
    def get_all_constants(self) -> tuple:
        return tuple(self._constants)
