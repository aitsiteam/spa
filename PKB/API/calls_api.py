class CallsAPI:
    def __init__(self):
        self._CallsTable = {}

    def __str__(self):
        return 'CallsTable: ' + str(self._CallsTable)

    # CallsTable
    # adds Calls relation to CallsTable
    def add_calls(self, procedure1: str, procedure2: str):
        try:
            self._CallsTable[procedure1].add(procedure2)
        except KeyError:
            new_record = {
                procedure1: {procedure2}
            }

            self._CallsTable.update(new_record)

    def get_record(self, procedure: str):
        return self._CallsTable[procedure]

    # Calls(_, p)
    # returns tuple of proc_name's (procedure names)
    # gets all procedures which directly calls procedure with name proc_name
    def get_calls(self, proc_name: str) -> tuple:
        return tuple(procedure for procedure in self._CallsTable if self.is_calls(procedure, proc_name))

    # Calls(p, _)
    # returns tuple of proc_name's (procedure names)
    # gets all procedures which are directly called from procedure with name proc_name
    def get_called_from(self, proc_name: str) -> tuple:
        try:
            return tuple(self.get_record(proc_name))
        except KeyError:
            return ()

    # Calls(p1, p2)
    # returns True/False
    # if procedure with name proc1_name calls directly procedure with name proc2_name
    def is_calls(self, procedure1: str, procedure2: str) -> bool:
        return procedure2 in self.get_called_from(procedure1)

    # Calls*(_, p)
    # returns tuple of proc_name's (procedure names)
    # gets all procedures which directly or indirectly calls procedure with name proc_name
    def get_calls_t(self, proc_name: str) -> tuple:
        return tuple(procedure for procedure in self._CallsTable if self.is_calls_t(procedure, proc_name))

    # Calls*(p, _)
    # returns tuple of proc_name's (procedure names)
    # gets all procedures which are directly or indirectly called from procedure with name proc_name
    def get_called_from_t(self, proc_name: str) -> tuple:
        procedures = set()

        for calls_list in self._CallsTable.values():
            for procedure in calls_list:
                if self.is_calls_t(proc_name, procedure):
                    procedures.add(procedure)

        return tuple(procedures)

    # Calls*(p1, p2)
    # returns True/False
    # if procedure with name proc1_name calls directly or indirectly procedure with name proc2_name
    def is_calls_t(self, proc1_name: str, proc2_name: str) -> bool:
        try:
            for procedure in self._CallsTable[proc1_name]:
                if procedure == proc2_name or self.is_calls_t(procedure, proc2_name):
                    return True
        except KeyError:
            return False
        return False
