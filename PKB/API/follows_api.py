class FollowsAPI:
    def __init__(self):
        self._FollowsTable = {}

    def __str__(self):
        return 'FollowsTable: ' + str(self._FollowsTable)

    # FollowsTable
    # adds Follows relation to FollowsTable
    def add_follows(self, prog_line1: int, prog_line2: int):
        new_record = {
            prog_line1: prog_line2
        }

        self._FollowsTable.update(new_record)

    def get_record(self, prog_line: int):
        return self._FollowsTable[prog_line]

    # Follows(s, _)
    # returns prog_line
    # gets stmt which is directly followed by stmt s
    def get_followed_by(self, prog_line: int) -> tuple:
        try:
            return (self.get_record(prog_line),)
        except KeyError:
            return ()

    # Follows*(s, _)
    # returns tuple of prog_lines
    # gets all stmts which are directly or indirectly followed by stmt s
    def get_followed_by_t(self, prog_line: int) -> tuple:
        result = [
            followed for follower, followed in self._FollowsTable.items()
            if self.is_follows_t(prog_line, followed)
        ]

        return tuple(result)

    # Follows(_, s)
    # returns prog_line
    # gets stmt which follows stmt s
    def get_follower(self, prog_line: int) -> tuple:
        for follower, followed in self._FollowsTable.items():
            if followed == prog_line:
                return tuple([follower])

        return tuple()

    # Follows*(_, s)
    # returns prog_line
    # gets all transitive followers of stmt s
    def get_followers_t(self, prog_line: int) -> tuple:
        result = [
            follower for follower, followed in self._FollowsTable.items()
            if self.is_follows_t(follower, prog_line)
        ]

        return tuple(result)

    # Follows(s1, s2)
    # returns True/False
    # if stmt s1 directly follows stmt s2
    def is_follows(self, prog_line1: int, prog_line2: int) -> bool:
        try:
            return self.get_record(prog_line1) == prog_line2
        except KeyError:
            return False

    # Follows*(s1, s2)
    # returns True/False
    # if stmt s1 directly or indirectly follows stmt s2
    def is_follows_t(self, prog_line1: int, prog_line2: int) -> bool:
        try:
            stmt = self.get_record(prog_line1)

            if stmt == prog_line2 or self.is_follows_t(stmt, prog_line2):
                return True

            return False
        except KeyError:
            return False
