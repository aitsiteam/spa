class VarTableRecord:
    def __init__(self):
        self.modified_in = set()
        self.used_in = set()

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        return 'Modified: {} | Used: {}'.format(self.modified_in, self.used_in)


class VarTableAPI:
    def __init__(self):
        self._var_table = {}

    def __str__(self):
        return 'VarTable: ' + str(self._var_table)

    def add_record(self, variable: str):
        if variable not in self._var_table:
            record = {
                variable: VarTableRecord()
            }

            self._var_table.update(record)

    def get_record(self, variable: str):
        return self._var_table[variable]

    def add_modified_in(self, variable: str, stmts: set):
        self.get_record(variable).modified_in.update(stmts)

    def add_used_in(self, variable: str, stmts: set):
        self.get_record(variable).used_in.update(stmts)

    def get_uses(self, prog_line: int) -> tuple:
        return tuple(variable for variable, record in self._var_table.items() if prog_line in record.used_in)

    def get_modifies(self, prog_line: int) -> tuple:
        return tuple(variable for variable, record in self._var_table.items() if prog_line in record.modified_in)

    def get_used_in(self, variable: str) -> tuple:
        try:
            return tuple(self.get_record(variable).used_in)
        except KeyError:
            return ()

    def get_modified_in(self, variable: str) -> tuple:
        try:
            return tuple(self.get_record(variable).modified_in)
        except KeyError:
            return ()

    def is_uses(self, prog_line: int, variable: str) -> bool:
        return prog_line in self.get_used_in(variable)

    def is_modifies(self, prog_line: int, variable: str) -> bool:
        return prog_line in self.get_modified_in(variable)

    # returns tuple of var_names
    # gets all variables from program
    def get_all_variables(self) -> tuple:
        return tuple(self._var_table.keys())
