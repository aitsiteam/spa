class ParentTableRecord:
    def __init__(self, parent_prog_line: int, upper_parents: set):
        self.parent = parent_prog_line
        self.parent_t = upper_parents

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        return 'Parent: {} | Parent*: {}'.format(self.parent, self.parent_t)


class ParentAPI:
    def __init__(self):
        self._ParentTable = {}

    def __str__(self):
        return 'ParentTable: ' + str(self._ParentTable)

    # ParentTable
    # adds Parent relation to ParentTable
    def add_parents(self, parent_prog_line: int, upper_parents: set, child_prog_line: int):
        new_record = {
            child_prog_line: ParentTableRecord(parent_prog_line, upper_parents)
        }

        self._ParentTable.update(new_record)

    def get_record(self, prog_line: int):
        return self._ParentTable[prog_line]

    # Parent(s, _)
    # returns tuple of prog_lines
    # gets all childs which their parent is stmt s with s.prog_line = prog_line
    def get_childs(self, prog_line: int) -> tuple:
        return tuple(stmt for stmt, record in self._ParentTable.items() if record.parent == prog_line)

    # Parent*(s, _)
    # returns tuple of prog_lines
    # gets all childs which their transitive parent is stmt s with s.prog_line = prog_line
    def get_childs_t(self, prog_line: int) -> tuple:
        return tuple(stmt for stmt, record in self._ParentTable.items() if prog_line in record.parent_t)

    # Parent(_, s)
    # returns prog_line
    # gets parent of stmt s with s.prog_line = prog_line
    def get_parent(self, prog_line: int) -> tuple:
        try:
            return (self.get_record(prog_line).parent,)
        except KeyError:
            return ()

    # Parent*(_, s)
    # returns prog_line
    # gets all transitive parents of stmt s with s.prog_line = prog_line
    def get_parent_t(self, prog_line: int) -> tuple:
        try:
            return tuple(self.get_record(prog_line).parent_t,)
        except KeyError:
            return ()

    # Parent(s1, s2)
    # returns True/False
    # if stmt s1 with s1.prog_line = prog_line1 is parent of stmt s2 with s2.prog_line = prog_line2
    def is_parent(self, prog_line1: int, prog_line2: int) -> bool:
        try:
            return self.get_record(prog_line2).parent == prog_line1
        except KeyError:
            return False

    # Parent*(s1, s2)
    # returns True/False
    # if stmt s1 with s1.prog_line = prog_line1 is transitive parent of stmt s2 with s2.prog_line = prog_line2
    def is_parent_t(self, prog_line1: int, prog_line2: int) -> bool:
        try:
            return prog_line1 in self.get_record(prog_line2).parent_t
        except KeyError:
            return False
