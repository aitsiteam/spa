from PKB.AST.nodes import ProcedureNode


class ProcTableRecord:
    def __init__(self, procedure_node: ProcedureNode):
        self.AST = procedure_node
        self.CFG = None     # Need?
        self.modifies = set()
        self.uses = set()

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        return 'AST node: {} | Modifies: {} | Uses: {}'.format(self.AST.__class__, self.modifies, self.uses)


class ProcTableAPI:
    def __init__(self):
        self._proc_table = {}

    def __str__(self):
        return 'ProcTable: ' + str(self._proc_table)

    def add_record(self, procedure_node: ProcedureNode):
        record = {
            procedure_node.name: ProcTableRecord(procedure_node)
        }

        self._proc_table.update(record)

    def get_record(self, procedure: str):
        return self._proc_table[procedure]

    def add_ast_root(self, procedure: str, node: ProcedureNode):
        self.get_record(procedure).AST = node

    def add_modifies(self, procedure: str, variable: str):
        self.get_record(procedure).modifies.add(variable)

    def add_modifies_range(self, procedure: str, variables: set):
        self.get_record(procedure).modifies.update(variables)

    def add_uses(self, procedure: str, variable: str):
        self.get_record(procedure).uses.add(variable)

    def add_uses_range(self, procedure: str, variables: set):
        self.get_record(procedure).uses.update(variables)

    def get_uses(self, procedure: str) -> tuple:
        try:
            return tuple(self.get_record(procedure).uses)
        except KeyError:
            return ()

    def get_modifies(self, procedure: str) -> tuple:
        try:
            return tuple(self.get_record(procedure).modifies)
        except KeyError:
            return ()

    def get_used_in(self, variable: str) -> tuple:
        return tuple(procedure for procedure, record in self._proc_table.items() if variable in record.uses)

    def get_modified_in(self, variable: str) -> tuple:
        return tuple(procedure for procedure, record in self._proc_table.items() if variable in record.modifies)

    def is_uses(self, procedure: str, variable: str) -> bool:
        return variable in self.get_uses(procedure)

    def is_modifies(self, procedure: str, variable: str) -> bool:
        return variable in self.get_modifies(procedure)

    # returns tuple of proc_names
    # gets all program procedures
    def get_all_procedures(self) -> tuple:
        return tuple(self._proc_table.keys())
