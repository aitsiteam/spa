class Node:
    def __init__(self):
        self.name = None


class StmtNode(Node):
    def __init__(self, prog_line: int):
        super().__init__()

        self.prog_line = prog_line

    def __repr__(self):
        return self.__str__()


class CollectionNode(Node):
    def __init__(self):
        self.stmt_lst = []

        super().__init__()

    def add_stmt(self, stmt: StmtNode):
        self.stmt_lst.append(stmt)


class ProcedureNode(CollectionNode):
    def __init__(self):
        super().__init__()

    def __str__(self):
        return 'procedure({},{})'.format(self.name, self.stmt_lst)


class ProgramNode:
    def __init__(self):
        self.procedures = []

    def get_procedure(self, name: str):
        return next((p for p in self.procedures if p.proc_name == name), None)

    def __str__(self):
        return 'program([{}])'.format(', '.join(str(p) for p in self.procedures))


class CallNode(StmtNode):
    def __init__(self, prog_line: int):
        super().__init__(prog_line)

    def __str__(self):
        return 'call(' + self.name + ')'


class WhileNode(StmtNode, CollectionNode):
    def __init__(self, prog_line: int):
        super().__init__(prog_line)

    def __str__(self):
        return 'while({},{})'.format(self.name, self.stmt_lst)


class IfNode(StmtNode):
    def __init__(self, prog_line: int):
        super().__init__(prog_line)

        self.if_then = True
        self.then_stmt_lst = []
        self.else_stmt_lst = []

    def __str__(self):
        return 'if({},{},{})'.format(self.name, self.then_stmt_lst, self.else_stmt_lst)

    def add_then_stmt(self, stmt: StmtNode):
        self.then_stmt_lst.append(stmt)

    def add_else_stmt(self, stmt: StmtNode):
        self.else_stmt_lst.append(stmt)


class OperatorNode:
    def __init__(self, operator: str, left_child, right_child):
        self.operator = operator
        self.left_child = left_child
        self.right_child = right_child

    def __str__(self):
        return '{}({},{})'.format(self.operator, self.left_child, self.right_child)


class AssignNode(StmtNode):
    def __init__(self, prog_line: int):
        super().__init__(prog_line)

        self.expr = 0

    def __str__(self):
        return 'assign({},{})'.format(self.name, self.expr)


class PlusNode(OperatorNode):
    def __init__(self, left_child, right_child):
        super().__init__('+', left_child, right_child)


class MinusNode(OperatorNode):
    def __init__(self, left_child, right_child):
        super().__init__('-', left_child, right_child)


class TimesNode(OperatorNode):
    def __init__(self, left_child, right_child):
        super().__init__('*', left_child, right_child)
