import tests.helpers.database_assignment_1 as database_assignment_1
from PQL.pql_parser import PqlParser
import os


class TestPQL:
    test_program_1 = database_assignment_1.get_database()

    def _get_parser_result(self, definitions, query):
        temporary_file_path = "temp_program.txt"
        with open(temporary_file_path, 'w') as temp_program_file:
            temp_program_file.write(self.test_program_1.program)
        p = PqlParser(temporary_file_path)
        p.run(definitions, query)
        os.remove(temporary_file_path)
        return p.final_result

    def _compare_result(self, cases):
        for case in cases:
            parser_result = self._get_parser_result(case.definitions, case.query)

            assert parser_result == case.answer

    def test_calls(self):
        cases = self.test_program_1.get_cases_with_calls()
        self._compare_result(cases)

    def test_calls_asterisk(self):
        cases = self.test_program_1.get_cases_with_calls_asterisk()
        self._compare_result(cases)

    def test_parent(self):
        cases = self.test_program_1.get_cases_with_parent()
        self._compare_result(cases)

    def test_parent_asterisk(self):
        cases = self.test_program_1.get_cases_with_parent_asterisk()
        self._compare_result(cases)

    def test_uses(self):
        cases = self.test_program_1.get_cases_with_uses()
        self._compare_result(cases)

    def test_modifies(self):
        cases = self.test_program_1.get_cases_with_modifies()
        self._compare_result(cases)

    def test_follows(self):
        cases = self.test_program_1.get_cases_with_follows()
        self._compare_result(cases)

    def test_follows_asterisk(self):
        cases = self.test_program_1.get_cases_with_follows_asterisk()
        self._compare_result(cases)

    def test_next(self):
        cases = self.test_program_1.get_cases_with_next()
        self._compare_result(cases)

    def test_next_asterisk(self):
        cases = self.test_program_1.get_cases_with_next_asterisk()
        self._compare_result(cases)


# test = TestPQL()
# test.test_next()
# test.test_next_asterisk()
# test.test_parent()
# test.test_parent_asterisk()
# test.test_uses()
# test.test_modifies()
# test.test_follows()
# test.test_follows_asterisk()
# test.test_next()
# test.test_next_asterisk()  # uncomment if you want to test manually