from PQL.pql_parser import PqlParser


class TestRelationsTree:
    @staticmethod
    def _are_other_relations_empty(current_relations, relations_tree):
        """
        Makes sure that relations other than these specified in a test program are empty
        :param current_relations: list of relations that exist in a test program
        :param relations_tree: instanec of relations tree
        """
        relations_list = [
            'Parent',
            'Parent*',
            'Uses',
            'Modifies',
            'Follows',
            'Follows*',
            'Next',
            'Next*',
            'Calls',
            'Calls*'
        ]
        for relation in relations_list:
            if relation in current_relations:
                continue
            assert relation in relations_tree
            assert isinstance(relations_tree[relation], list)
            assert len(relations_tree[relation]) == 0

    @staticmethod
    def _get_test_variables():
        return 'assign a; variable v;  constant c; constant d; procedure p; stmt s;prog_line n;if i'

    def test_parent_relations(self):
        test_program = 'Select a such that Parent   (   v  ,   "b"     ) and Parent (1, 4)' \
                       ' with v.varName =    "First"  pattern a   (_"c"_,_) with p.procName   = "a2" with c.value   = 2 ' \
                       'with s.stmt# = c.value with n=1'

        parser = PqlParser()
        parser.run(self._get_test_variables(), test_program)
        relations_tree = parser.relations_tree
        assert isinstance(relations_tree, dict)
        self._are_other_relations_empty(['Parent'], relations_tree)
        assert 'Parent' in relations_tree
        parent_list = relations_tree['Parent']
        assert isinstance(parent_list, list)
        assert len(parent_list) == 2
        assert isinstance(parent_list[0], dict)
        assert parent_list[0]['par_1'] == '"First"'
        assert parent_list[0]['par_2'] == '"b"'

        assert isinstance(parent_list[1], dict)
        assert parent_list[1]['par_1'] == '1'
        assert parent_list[1]['par_2'] == '4'

    def test_parent_asterisk_relations(self):
        test_program = 'Select a such that Parent   (   v  ,   "b"     ) and Parent* (2, 3) and Parent*(n, 2)' \
                       ' with v.varName =    "First"  pattern a   (_"c"_,_) with p.procName   = "a2" with c.value   = 2 ' \
                       'with s.stmt# = c.value with n=1'

        parser = PqlParser()
        parser.run(self._get_test_variables(), test_program)
        relations_tree = parser.relations_tree
        assert isinstance(relations_tree, dict)
        self._are_other_relations_empty(['Parent', 'Parent*'], relations_tree)
        assert 'Parent' in relations_tree
        parent_list = relations_tree['Parent']
        assert isinstance(parent_list, list)
        assert len(parent_list) == 1
        assert isinstance(parent_list[0], dict)
        assert parent_list[0]['par_1'] == '"First"'
        assert parent_list[0]['par_2'] == '"b"'

        assert 'Parent*' in relations_tree
        parent_asterisk_list = relations_tree['Parent*']
        assert isinstance(parent_asterisk_list, list)
        assert len(parent_asterisk_list) == 2
        assert isinstance(parent_asterisk_list[0], dict)
        assert parent_asterisk_list[0]['par_1'] == '2'
        assert parent_asterisk_list[0]['par_2'] == '3'

        assert isinstance(parent_list[1], dict)
        assert parent_list[1]['par_1'] == '1'
        assert parent_list[1]['par_2'] == '2'

    def test_follows_relations(self):
        test_program = 'Select a such that Follows(1, 4) and Follows (c, 5)' \
                       ' with v.varName =    "First"  pattern a   (_"c"_,_) with p.procName   = "a2" with c.value   = 2 ' \
                       'with s.stmt# = c.value with n=1'

        parser = PqlParser()
        parser.run(self._get_test_variables(), test_program)
        relations_tree = parser.relations_tree
        assert isinstance(relations_tree, dict)
        self._are_other_relations_empty(['Follows'], relations_tree)
        assert 'Follows' in relations_tree
        follows_list = relations_tree['Follows']
        assert isinstance(follows_list, list)
        assert len(follows_list) == 2
        assert isinstance(follows_list[0], dict)
        assert follows_list[0]['par_1'] == '1'
        assert follows_list[0]['par_2'] == '4'

        assert isinstance(follows_list[1], dict)
        assert follows_list[1]['par_1'] == '2'
        assert follows_list[1]['par_2'] == '5'

    def test_follows_asterisk_relations(self):
        test_program = 'Select a such that Follows(1, 4) and Follows* (c, 5) and Follows*(2, 3)' \
                       ' with v.varName =    "First"  pattern a   (_"c"_,_) with p.procName   = "a2" with c.value   = 2 ' \
                       'with s.stmt# = c.value with n=1'

        parser = PqlParser()
        parser.run(self._get_test_variables(), test_program)
        relations_tree = parser.relations_tree
        self._are_other_relations_empty(['Follows', 'Follows*'], relations_tree)
        assert isinstance(relations_tree, dict)
        assert 'Follows' in relations_tree
        follows_list = relations_tree['Follows']
        assert isinstance(follows_list, list)
        assert len(follows_list) == 1
        assert isinstance(follows_list[0], dict)
        assert follows_list[0]['par_1'] == '1'
        assert follows_list[0]['par_2'] == '4'

        assert 'Follows*' in relations_tree
        follows_asterisk_list = relations_tree['Follows*']
        assert isinstance(follows_asterisk_list, list)
        assert len(follows_asterisk_list) == 2
        assert isinstance(follows_asterisk_list[0], dict)
        assert follows_asterisk_list[0]['par_1'] == '1'
        assert follows_asterisk_list[0]['par_2'] == '5'

        assert isinstance(follows_asterisk_list[1], dict)
        assert follows_asterisk_list[1]['par_1'] == '2'
        assert follows_asterisk_list[1]['par_2'] == '3'

    def test_next_relations(self):
        test_program = 'Select a such that Next(v, 4) and Next   (3, 5)' \
                       ' with v.varName =    "First"  pattern a   (_"c"_,_) with p.procName   = "a2" with c.value   = 2 ' \
                       'with s.stmt# = c.value with n=1'

        parser = PqlParser()
        parser.run(self._get_test_variables(), test_program)
        relations_tree = parser.relations_tree
        assert isinstance(relations_tree, dict)
        self._are_other_relations_empty(['Next'], relations_tree)
        assert 'Next' in relations_tree
        next_list = relations_tree['Next']
        assert isinstance(next_list, list)
        assert len(next_list) == 2
        assert isinstance(next_list[0], dict)
        assert next_list[0]['par_1'] == '"First"'
        assert next_list[0]['par_2'] == '4'

        assert isinstance(next_list[1], dict)
        assert next_list[1]['par_1'] == '3'
        assert next_list[1]['par_2'] == '5'

    def test_next_asterisk_relations(self):
        test_program = 'Select a such that Next(1, 4) and Next* (c, 5) and Next*(2, 3)' \
                       ' with v.varName =    "First"  pattern a   (_"c"_,_) with p.procName   = "a2" with c.value   = 2 ' \
                       'with s.stmt# = c.value with n=1'

        parser = PqlParser()
        parser.run(self._get_test_variables(), test_program)
        relations_tree = parser.relations_tree
        assert isinstance(relations_tree, dict)
        self._are_other_relations_empty(['Next', 'Next*'], relations_tree)
        assert 'Next' in relations_tree
        next_list = relations_tree['Next']
        assert isinstance(next_list, list)
        assert len(next_list) == 1
        assert isinstance(next_list[0], dict)
        assert next_list[0]['par_1'] == '1'
        assert next_list[0]['par_2'] == '4'

        assert 'Next*' in relations_tree
        next_asterisk_list = relations_tree['Next*']
        assert isinstance(next_asterisk_list, list)
        assert len(next_asterisk_list) == 2
        assert isinstance(next_asterisk_list[0], dict)
        assert next_asterisk_list[0]['par_1'] == '1'
        assert next_asterisk_list[0]['par_2'] == '5'

        assert isinstance(next_asterisk_list[1], dict)
        assert next_asterisk_list[1]['par_1'] == '2'
        assert next_asterisk_list[1]['par_2'] == '3'

    def test_calls_relations(self):
        test_program = 'Select a such that Calls(3, 4) and Calls (4, 6)' \
                       ' with v.varName =    "First"  pattern a   (_"c"_,_) with p.procName   = "a2" with c.value   = 2 ' \
                       'with s.stmt# = c.value with n=1'

        parser = PqlParser()
        parser.run(self._get_test_variables(), test_program)
        relations_tree = parser.relations_tree
        assert isinstance(relations_tree, dict)
        self._are_other_relations_empty(['Calls'], relations_tree)
        assert 'Calls' in relations_tree
        calls_list = relations_tree['Calls']
        assert isinstance(calls_list, list)
        assert len(calls_list) == 2
        assert isinstance(calls_list[0], dict)
        assert calls_list[0]['par_1'] == '3'
        assert calls_list[0]['par_2'] == '4'

        assert isinstance(calls_list[1], dict)
        assert calls_list[1]['par_1'] == '4'
        assert calls_list[1]['par_2'] == '6'

    def test_calls_asterisk_relations(self):
        test_program = 'Select a such that Calls(1, 4) and Calls* (c, 5) and Calls*(2, 3)' \
                       ' with v.varName =    "First"  pattern a   (_"c"_,_) with p.procName   = "a2" with c.value   = 2 ' \
                       'with s.stmt# = c.value with n=1'

        parser = PqlParser()
        parser.run(self._get_test_variables(), test_program)
        relations_tree = parser.relations_tree
        assert isinstance(relations_tree, dict)
        self._are_other_relations_empty(['Calls', 'Calls*'], relations_tree)
        assert 'Calls' in relations_tree
        calls_list = relations_tree['Calls']
        assert isinstance(calls_list, list)
        assert len(calls_list) == 1
        assert isinstance(calls_list[0], dict)
        assert calls_list[0]['par_1'] == '1'
        assert calls_list[0]['par_2'] == '4'

        assert 'Calls*' in relations_tree
        calls_asterisk_list = relations_tree['Calls*']
        assert isinstance(calls_asterisk_list, list)
        assert len(calls_asterisk_list) == 2
        assert isinstance(calls_asterisk_list[0], dict)
        assert calls_asterisk_list[0]['par_1'] == '1'
        assert calls_asterisk_list[0]['par_2'] == '5'

        assert isinstance(calls_asterisk_list[1], dict)
        assert calls_asterisk_list[1]['par_1'] == '2'
        assert calls_asterisk_list[1]['par_2'] == '3'

    def test_uses_relations(self):
        test_program = 'Select a such that Uses(5, 7) and Uses   (8,   11)' \
                       ' with v.varName =    "First"  pattern a   (_"c"_,_) with p.procName   = "a2" with c.value   = 2 ' \
                       'with s.stmt# = c.value with n=1'

        parser = PqlParser()
        parser.run(self._get_test_variables(), test_program)
        relations_tree = parser.relations_tree
        assert isinstance(relations_tree, dict)
        self._are_other_relations_empty(['Uses'], relations_tree)
        assert 'Uses' in relations_tree
        uses_list = relations_tree['Uses']
        assert isinstance(uses_list, list)
        assert len(uses_list) == 2
        assert isinstance(uses_list[0], dict)
        assert uses_list[0]['par_1'] == '5'
        assert uses_list[0]['par_2'] == '7'

        assert isinstance(uses_list[1], dict)
        assert uses_list[1]['par_1'] == '8'
        assert uses_list[1]['par_2'] == '11'

    def test_modifies_relations(self):
        test_program = 'Select a such that Modifies(2, 3) and Modifies (5, 6)' \
                       ' with v.varName =    "First"  pattern a   (_"c"_,_) with p.procName   = "a2" with c.value   = 2 ' \
                       'with s.stmt# = c.value with n=1'

        parser = PqlParser()
        parser.run(self._get_test_variables(), test_program)
        relations_tree = parser.relations_tree
        assert isinstance(relations_tree, dict)
        self._are_other_relations_empty(['Modifies'], relations_tree)
        assert 'Modifies' in relations_tree
        modifies_list = relations_tree['Modifies']
        assert isinstance(modifies_list, list)
        assert len(modifies_list) == 2
        assert isinstance(modifies_list[0], dict)
        assert modifies_list[0]['par_1'] == '2'
        assert modifies_list[0]['par_2'] == '3'

        assert isinstance(modifies_list[1], dict)
        assert modifies_list[1]['par_1'] == '5'
        assert modifies_list[1]['par_2'] == '6'

    def test_with_relations(self):
        test_program = 'Select a such that Follows(1, 4) and Follows (c, 5)' \
                       ' with v.varName =    "First"  pattern a   (_"c"_,_) with p.procName   = "a2" with c.value   = 2 ' \
                       'with s.stmt# = c.value with n=1'

        parser = PqlParser()
        parser.run(self._get_test_variables(), test_program)
        relations_tree = parser.relations_tree
        assert isinstance(relations_tree, dict)
        assert 'with' in relations_tree
        with_list = relations_tree['with']
        assert isinstance(with_list, list)
        assert len(with_list) == 4
        assert isinstance(with_list[0], dict)
        assert with_list[0]['variable'] == 'v'
        assert with_list[0]['value'] == '"First"'

        assert isinstance(with_list[1], dict)
        assert with_list[1]['variable'] == 'p'
        assert with_list[1]['value'] == '"a2"'

        assert isinstance(with_list[2], dict)
        assert with_list[2]['variable'] == 's'
        assert with_list[2]['value'] == 'c.value'

        assert isinstance(with_list[3], dict)
        assert with_list[3]['variable'] == 'n'
        assert with_list[3]['value'] == '1'

    def test_pattern_relations(self):
        # TODO: Fix pattern detection as only one pattern is detected for now
        test_program = 'Select a such that Follows(1, 4) and Follows (c, 5)' \
                       ' with v.varName =    "First"  pattern a   (_"c"_,_) pattern g (_"test"_, __) ' \
                       'with p.procName   = "a2" with c.value   = 2 ' \
                       'with s.stmt# = c.value with n=1'

        parser = PqlParser()
        parser.run(self._get_test_variables(), test_program)
        relations_tree = parser.relations_tree
        assert isinstance(relations_tree, dict)
        assert 'pattern' in relations_tree
        pattern_list = relations_tree['pattern']
        assert isinstance(pattern_list, list)
        assert len(pattern_list) == 2
        assert isinstance(pattern_list[0], dict)
        assert pattern_list[0]['variable'] == 'a'
        assert pattern_list[0]['par_1'] == '_"c"_'
        assert pattern_list[0]['par_2'] == '_'

        assert isinstance(pattern_list[1], dict)
        assert pattern_list[1]['variable'] == 'g'
        assert pattern_list[1]['par_1'] == '_"test"_'
        assert pattern_list[1]['par_2'] == '__'
