import os
import re
from random import randint

from PKB.Parser.parser import parse


class TestPKB:
    test_runs = 10  # How many times should the modified version run
    max_spaces = 10  # Max amount of spaces to be randomly added to simple code

    def _get_get_calls(self, table):
        calls = {}
        for index, value in table.items():
            for i in value:
                calls.setdefault(i, set()).add(index)
        return calls

    def _get_called_from(self, table):
        return table  # Well, pretty stupid but yeah

    def _is_calls(self, table, a, b):
        keys = table.keys()
        if a not in keys:
            return False
        try:
            tested_list = table[a]
            return b in tested_list
        except KeyError:
            return False

    def _generate_modified_file(self, file_name):
        simple_symbols = ['{', '}', 'if', 'else', 'then', 'call', ';', 'assign', 'procedure', 'while', '=', '\+', '-',
                          '\(', '\)', '\*']
        with open(file_name, 'r') as file:
            content = file.read()
        for symbol in simple_symbols:
            content_list = list(content)
            symbol_positions = [m.start() for m in re.finditer(symbol, content)]
            inserted_spaces = 0
            for symbol_position in symbol_positions:
                spaces_to_insert = randint(0, self.max_spaces)
                content_list.insert(symbol_position + inserted_spaces, ' ' * spaces_to_insert)
                inserted_spaces += 1
                # inserted_spaces += spaces_to_insert
            content = ''.join(content_list)
        temporary_file_path = f'{file_name}_temp'
        with open(temporary_file_path, 'w') as temp_modfied_file:
            temp_modfied_file.write(content)
        return temporary_file_path

    def _start_testing_calls(self, pkb, proper_calls_table):
        calls_api = pkb.Calls
        calls_table = calls_api._CallsTable  # Accessing a private variable, but who cares
        assert calls_table == proper_calls_table
        proper_get_calls = self._get_get_calls(proper_calls_table)
        for procedure in calls_table.values():
            for item in procedure:
                api_get_calls_result = calls_api.get_calls(item)
                proper_get_calls_result = proper_get_calls[item]
                assert set(api_get_calls_result) == proper_get_calls_result

        proper_get_called_from = self._get_called_from(proper_calls_table)
        for index, value in calls_table.items():
            api_get_called_from_result = calls_api.get_called_from(index)
            proper_get_called_from_result = proper_get_called_from[index]
            assert set(api_get_called_from_result) == set(proper_get_called_from_result)

        for index, value in calls_table.items():
            for procedure in value:
                api_get_is_calls_result_first = calls_api.is_calls(index, procedure)
                proper_get_is_calls_result_first = self._is_calls(proper_calls_table, index, procedure)
                assert api_get_is_calls_result_first is proper_get_is_calls_result_first

    def test_normal_program0(self, file_name=None):
        if not file_name:
            dir_name = os.path.dirname(__file__)
            program_number = 0
            file_name = f'{dir_name}/static/program{program_number}.txt'  # TODO: move test files to one directory

        pkb = parse(file_name)
        proper_calls_table = {
            'test': [
                'a',
                'h',
            ],
            'test2': [
                'c'
            ]
        }

        self._start_testing_calls(pkb, proper_calls_table)

    def test_normal_program1(self, file_name=None):
        if not file_name:
            dir_name = os.path.dirname(__file__)
            program_number = 1
            file_name = f'{dir_name}/static/program{program_number}.txt'  # TODO: move test files to one directory

        pkb = parse(file_name)
        proper_calls_table = {
            'Circle': [
                'Triangle',
                'Hexagon',
                'Hexagon',
                'Rectangle'
            ],
            'Rectangle': [
                'Triangle'
            ]
        }

        self._start_testing_calls(pkb, proper_calls_table)

    def test_normal_program2(self, file_name=None):
        if not file_name:
            dir_name = os.path.dirname(__file__)
            program_number = 2
            file_name = f'{dir_name}/static/program{program_number}.txt'  # TODO: move test files to one directory

        pkb = parse(file_name)
        proper_calls_table = {
            'Uno': [
                'Dos',
                'Cuatro',
            ],
            'Dos': [
                'Tres'
            ]
        }

        self._start_testing_calls(pkb, proper_calls_table)

    def test_normal_program4(self, file_name=None):
        if not file_name:
            dir_name = os.path.dirname(__file__)
            program_number = 4
            file_name = f'{dir_name}/static/program{program_number}.txt'  # TODO: move test files to one directory

        pkb = parse(file_name)
        proper_calls_table = {
            'A': [
                'B',
            ],
            'B': [
                'C',
                'D'
            ],
            'C': [
                'D',
                'E'
            ],
            'E': [
                'F'
            ]
        }

        self._start_testing_calls(pkb, proper_calls_table)

    def test_normal_program5(self, file_name=None):
        if not file_name:
            dir_name = os.path.dirname(__file__)
            program_number = 5
            file_name = f'{dir_name}/static/program{program_number}.txt'  # TODO: move test files to one directory

        pkb = parse(file_name)
        proper_calls_table = {
            'test': [
                'z',
                'h'
            ],
        }

        self._start_testing_calls(pkb, proper_calls_table)

    def test_normal_program6(self, file_name=None):
        if not file_name:
            dir_name = os.path.dirname(__file__)
            program_number = 6
            file_name = f'{dir_name}/static/program{program_number}.txt'  # TODO: move test files to one directory

        pkb = parse(file_name)
        proper_calls_table = {
            'test': [
                'z',
                'a',
                'f',
                'h',
                'z',
                'x'
            ],
            'test2': [
                'c'
            ]
        }

        self._start_testing_calls(pkb, proper_calls_table)

    def test_normal_program7(self, file_name=None):
        if not file_name:
            dir_name = os.path.dirname(__file__)
            program_number = 7
            file_name = f'{dir_name}/static/program{program_number}.txt'  # TODO: move test files to one directory

        pkb = parse(file_name)
        proper_calls_table = {}

        self._start_testing_calls(pkb, proper_calls_table)

    def test_normal_program8(self, file_name=None):
        if not file_name:
            dir_name = os.path.dirname(__file__)
            program_number = 8
            file_name = f'{dir_name}/static/program{program_number}.txt'  # TODO: move test files to one directory

        pkb = parse(file_name)
        proper_calls_table = {
            'A': [
                'a',
                'b',
                'c',
                'd',
                's',
                'u'
            ],
        }

        self._start_testing_calls(pkb, proper_calls_table)

    def test_randomly_modified_program0(self):
        dir_name = os.path.dirname(__file__)
        program_number = 0
        file_name = f'{dir_name}/static/program{program_number}.txt'  # TODO: move test files to one directory

        for i in range(self.test_runs):
            temporary_file_path = self._generate_modified_file(file_name)
            self.test_normal_program0(temporary_file_path)

    def test_randomly_modified_program1(self):
        dir_name = os.path.dirname(__file__)
        program_number = 1
        file_name = f'{dir_name}/static/program{program_number}.txt'  # TODO: move test files to one directory

        for i in range(self.test_runs):
            temporary_file_path = self._generate_modified_file(file_name)
            self.test_normal_program1(temporary_file_path)

    def test_randomly_modified_program2(self):
        dir_name = os.path.dirname(__file__)
        program_number = 2
        file_name = f'{dir_name}/static/program{program_number}.txt'  # TODO: move test files to one directory

        for i in range(self.test_runs):
            temporary_file_path = self._generate_modified_file(file_name)
            self.test_normal_program2(temporary_file_path)

    def test_randomly_modified_program4(self):
        dir_name = os.path.dirname(__file__)
        program_number = 4
        file_name = f'{dir_name}/static/program{program_number}.txt'  # TODO: move test files to one directory

        for i in range(self.test_runs):
            temporary_file_path = self._generate_modified_file(file_name)
            self.test_normal_program4(temporary_file_path)

    def test_randomly_modified_program5(self):
        dir_name = os.path.dirname(__file__)
        program_number = 5
        file_name = f'{dir_name}/static/program{program_number}.txt'  # TODO: move test files to one directory

        for i in range(self.test_runs):
            temporary_file_path = self._generate_modified_file(file_name)
            self.test_normal_program5(temporary_file_path)

    def test_randomly_modified_program6(self):
        dir_name = os.path.dirname(__file__)
        program_number = 6
        file_name = f'{dir_name}/static/program{program_number}.txt'  # TODO: move test files to one directory

        for i in range(self.test_runs):
            temporary_file_path = self._generate_modified_file(file_name)
            self.test_normal_program6(temporary_file_path)

    def test_randomly_modified_program7(self):
        dir_name = os.path.dirname(__file__)
        program_number = 7
        file_name = f'{dir_name}/static/program{program_number}.txt'  # TODO: move test files to one directory

        for i in range(self.test_runs):
            temporary_file_path = self._generate_modified_file(file_name)
            self.test_normal_program7(temporary_file_path)

    def test_randomly_modified_program8(self):
        dir_name = os.path.dirname(__file__)
        program_number = 8
        file_name = f'{dir_name}/static/program{program_number}.txt'  # TODO: move test files to one directory

        for i in range(self.test_runs):
            temporary_file_path = self._generate_modified_file(file_name)
            self.test_normal_program8(temporary_file_path)
