
Select BOOLEAN such that Calls("Circle","Triangle")
true

Select BOOLEAN such that Calls("Circle","Hexagon")
true

Select BOOLEAN such that Calls("Circle","Rectangle")
true

Select BOOLEAN such that Calls("Rectangle","Circle")
false

Select BOOLEAN such that Calls("Rectangle","Triangle")
true

Select BOOLEAN such that Calls("Rectangle","Hexagon")
false

Select BOOLEAN such that Calls("Triangle","Circle")
false

Select BOOLEAN such that Calls("Triangle","Rectangle")
false

Select BOOLEAN such that Calls("Triangle","Hexagon")
false

Select BOOLEAN such that Calls("Hexagon","Circle")
false

Select BOOLEAN such that Calls("Hexagon","Rectangle")
false

Select BOOLEAN such that Calls("Hexagon","Triangle")
false
procedure p;
Select p such that Calls(p, "Circle")
none
procedure p;
Select p such that Calls(p, "Rectangle")
Circle
procedure p;
Select p such that Calls(p, "Triangle")
Circle, Rectangle
procedure p;
Select p such that Calls(p, "Hexagon")
Circle
procedure p;
Select p such that Calls(_, p)
Hexagon, Rectangle, Triangle
procedure p;
Select p such that Calls(p, _)
Circle, Rectangle

Select BOOLEAN such that Calls*("Circle","Triangle")
true

Select BOOLEAN such that Calls*("Circle","Hexagon")
true

Select BOOLEAN such that Calls*("Circle","Rectangle")
true

Select BOOLEAN such that Calls*("Rectangle","Circle")
false

Select BOOLEAN such that Calls*("Rectangle","Triangle")
true

Select BOOLEAN such that Calls*("Rectangle","Hexagon")
false

Select BOOLEAN such that Calls*("Triangle","Circle")
false

Select BOOLEAN such that Calls*("Triangle","Rectangle")
false

Select BOOLEAN such that Calls*("Triangle","Hexagon")
false

Select BOOLEAN such that Calls*("Hexagon","Circle")
false

Select BOOLEAN such that Calls*("Hexagon","Rectangle")
false

Select BOOLEAN such that Calls*("Hexagon","Triangle")
false
procedure p;
Select p such that Calls*(p, "Circle")
none
procedure p;
Select p such that Calls*(p, "Rectangle")
Circle
procedure p;
Select p such that Calls*(p, "Triangle")
Circle, Rectangle
procedure p;
Select p such that Calls*(p, "Hexagon")
Circle
procedure p;
Select p such that Calls*(_, p)
Hexagon, Rectangle, Triangle
procedure p;
Select p such that Calls*(p, _)
Circle, Rectangle