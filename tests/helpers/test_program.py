class TestProgram:
    """
    This is a class that's used to create test databases. Every TestProgram has a program (text string) and test cases,
    which are tests with their statements, query and question
    """

    def __init__(self, program, test_cases):
        """
        :param program: String containing the test program
        :param test_cases: List of test cases
        """
        self.test_cases = test_cases
        self.program = program

    def get_cases_with_calls(self):
        return [i for i in self.test_cases if i.calls]

    def get_cases_with_calls_asterisk(self):
        return [i for i in self.test_cases if i.calls_asterisk]

    def get_cases_with_parent(self):
        return [i for i in self.test_cases if i.parent]

    def get_cases_with_parent_asterisk(self):
        return [i for i in self.test_cases if i.parent_asterisk]

    def get_cases_with_uses(self):
        return [i for i in self.test_cases if i.uses]

    def get_cases_with_modifies(self):
        return [i for i in self.test_cases if i.modifies]

    def get_cases_with_follows(self):
        return [i for i in self.test_cases if i.follows]

    def get_cases_with_follows_asterisk(self):
        return [i for i in self.test_cases if i.follows_asterisk]

    def get_cases_with_next(self):
        return [i for i in self.test_cases if i.next]

    def get_cases_with_next_asterisk(self):
        return [i for i in self.test_cases if i.next_asterisk]
