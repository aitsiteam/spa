from PQL.result_tree import NONE
from .test_case import TestCase
from .test_program import TestProgram


def get_krzysztof_cases():
    return [
        # q1
        TestCase("stmt s, s1;", "Select s such that Parent (s, s1) with s1.stmt# = 2", NONE),
        TestCase("stmt s, s1;", "Select s such that Parent (s, s1) with s1.stmt# = 10", "8"),
        TestCase("stmt s, s1;", "Select s such that Parent (s, s1) with s1.stmt# = 11", "10"),
        TestCase("stmt s, s1;", "Select s such that Parent (s, s1) with s1.stmt# = 20", "18"),

        # q2
        TestCase("stmt s;", "Select s such that Parent (s, 2)", NONE),
        TestCase("stmt s;", "Select s such that Parent (s, 10)", "8"),
        TestCase("stmt s;", "Select s such that Parent (s, 11)", "10"),
        TestCase("stmt s;", "Select s such that Parent (s, 20)", "18"),

        # q3
        TestCase("stmt s;", "Select s such that Parent (2, s)", NONE),
        TestCase("stmt s;", "Select s such that Parent (8, s)", "9, 10, 13, 14, 15, 16"),
        TestCase("stmt s;", "Select s such that Parent (9, s)", NONE),
        TestCase("stmt s;", "Select s such that Parent (25, s)", NONE),

        # q4
        TestCase("stmt s;", "Select s such that Parent* (s, 2)", NONE),
        TestCase("stmt s;", "Select s such that Parent* (s, 10)", "8"),
        TestCase("stmt s;", "Select s such that Parent* (s, 11)", "8, 10"),
        TestCase("stmt s;", "Select s such that Parent* (s, 20)", "18"),

        # q5
        TestCase("stmt s; while w;", "Select w such that Parent* (s, 2)", NONE),
        TestCase("stmt s; while w;", "Select w such that Parent* (s, 10)", "10, 18, 23"),
        TestCase("stmt s; while w;", "Select w such that Parent* (s, 11)", "10, 18, 23"),
        TestCase("stmt s; while w;", "Select w such that Parent* (s, 20)", "10, 18, 23"),

        # q6
        TestCase("while w;", "Select w such that Parent* (w, 9)", NONE),
        TestCase("while w;", "Select w such that Parent* (w, 11)", "10"),
        TestCase("while w;", "Select w such that Parent* (w, 13)", NONE),
        TestCase("while w;", "Select w such that Parent* (w, 21)", "18"),

        # q7
        TestCase("stmt s;", "Select s such that Follows (s, 1)", NONE),
        TestCase("stmt s;", "Select s such that Follows (s, 8)", "7"),
        TestCase("stmt s;", "Select s such that Follows (s, 9)", NONE),
        TestCase("stmt s;", "Select s such that Follows (s, 10)", "9"),
        TestCase("stmt s;", "Select s such that Follows (s, 12)", "11"),
        TestCase("stmt s;", "Select s such that Follows (s, 13)", "10"),
        TestCase("stmt s;", "Select s such that Follows (s, 23)", NONE),

        # q7
        TestCase("stmt s;", "Select s such that Follows (s, 1)", NONE),
        TestCase("stmt s;", "Select s such that Follows (s, 8)", "7"),
        TestCase("stmt s;", "Select s such that Follows (s, 9)", NONE),
        TestCase("stmt s;", "Select s such that Follows (s, 10)", "9"),
        TestCase("stmt s;", "Select s such that Follows (s, 12)", "11"),
        TestCase("stmt s;", "Select s such that Follows (s, 13)", "10"),
        TestCase("stmt s;", "Select s such that Follows (s, 23)", NONE),

        # q8
        TestCase("assign a;", "Select a such that Follows (a, 1)", NONE),
        TestCase("assign a;", "Select a such that Follows (a, 8)", "7"),
        TestCase("assign a;", "Select a such that Follows (a, 9)", NONE),
        TestCase("assign a;", "Select a such that Follows (a, 10)", "9"),
        TestCase("assign a;", "Select a such that Follows (a, 12)", "11"),
        TestCase("assign a;", "Select a such that Follows (a, 13)", NONE),

        # q9
        TestCase("while w; stmt s;", "Select w such that Follows* (s, w)", "10"),

        # q10
        TestCase("while w; stmt s;", "Select w such that Follows* (w, s)", "10, 18, 23"),

        # q11
        TestCase("stmt s;", "Select s such that Follows* (s, 1)", NONE),
        TestCase("stmt s;", "Select s such that Follows* (s, 8)", "1, 2, 3, 4, 5, 6, 7"),
        TestCase("stmt s;", "Select s such that Follows* (s, 9)", NONE),
        TestCase("stmt s;", "Select s such that Follows* (s, 13)", "9, 10"),
        TestCase("stmt s;", "Select s such that Follows* (s, 19)", NONE),
        TestCase("stmt s;", "Select s such that Follows* (s, 22)", "18"),

        # q12
        TestCase("if ifstat;", "Select ifstat such that Follows* (ifstat, 8)", NONE),
        TestCase("if ifstat;", "Select ifstat such that Follows* (ifstat, 17)", "8"),
        TestCase("if ifstat;", "Select ifstat such that Follows* (ifstat, 25)", NONE),
        TestCase("if ifstat;", "Select ifstat such that Follows* (ifstat, 27)", NONE),

        # q13
        TestCase("assign a;", "Select a such that Follows* (a, 6)", "1, 2, 3, 5"),
        TestCase("assign a;", "Select a such that Follows* (a, 9)", NONE),
        TestCase("assign a;", "Select a such that Follows* (a, 10)", "9"),
        TestCase("assign a;", "Select a such that Follows* (a, 12)", "11"),
        TestCase("assign a;", "Select a such that Follows* (a, 17)", "1, 2, 3, 5, 7"),
        TestCase("assign a;", "Select a such that Follows* (a, 28)", NONE),

        # q14
        TestCase("variable v;", "Select v such that Modifies (3, v)", "d"),
        TestCase("variable v;", "Select v such that Modifies (4, v)", "a, c, d"),
        TestCase("variable v;", "Select v such that Modifies (6, v)", "t"),
        TestCase("variable v;", "Select v such that Modifies (24, v)", "a, d"),
        TestCase("variable v;", "Select v such that Modifies (28, v)", "t"),

        # q15
        TestCase("while w;", 'Select w such that Modifies (w, "d")', "10, 18, 23"),
        TestCase("while w;", 'Select w such that Modifies (w, "c")', "10, 18"),

        # q16
        TestCase("variable v;", 'Select v such that Modifies ("Rectangle", v)', "a, c, d, t"),

        # q17
        TestCase("stmt s;", 'Select s such that Uses (s, "d")',
                 "4, 8, 9, 10, 11, 12, 13, 14, 17, 18, 19, 20, 23, 24, 26, 27"),
        TestCase("stmt s;", 'Select s such that Uses (s, "c")', "8, 10, 16, 17, 18, 19, 21"),

        # q18
        TestCase("variable v;", "Select v such that Uses (10, v)", "c, d, t"),
        TestCase("variable v;", "Select v such that Uses (18, v)", "a, b, c, d, k, t"),
        TestCase("variable v;", "Select v such that Uses (23, v)", "a, b, d, k, t"),

        # q19
        TestCase("assign a; variable v;", "Select v such that Uses (a, v)", "a, b, c, d, k, t"),

        # q20
        TestCase("assign a;", 'Select a such that Modifies (a, "a") and Uses (a, "a")', "26"),
        TestCase("assign a;", 'Select a such that Modifies (a, "d") and Uses (a, "d")', "11"),
        TestCase("assign a;", 'Select a such that Modifies (a, "b") and Uses (a, "b")', NONE),
        TestCase("assign a;", 'Select a such that Modifies (a, "c") and Uses (a, "c")', "16, 21"),

        # q21
        TestCase("while w; assign a;", 'Select a such that Modifies (a, "t") and Parent (w, a)', "19"),

        # q22
        TestCase("while w; assign a;", 'Select a such that Parent (w, a) and Modifies (a, "t")', "19"),

        # q23
        TestCase("while w; assign a;", 'Select a such that Modifies (a, "t") such that Parent (w, a)', "19"),

        # q24
        TestCase("procedure p;", 'Select p such that Calls* (p, "Triangle")', "Circle, Rectangle"),

        # q25
        TestCase("procedure p;", 'Select p such that Calls ("Circle", p) and Modifies (p, "c") and Uses (p, "a")',
                 "Rectangle, Triangle"),
    ]


def get_iza_cases():
    return [
        # q50
        # TestCase("assign a2;", 'Select a2 such that Affects* (5, a2)', NONE),
        # TestCase("assign a2;", 'Select a2 such that Affects* (19, a2)', "22"),
        # TestCase("assign a2;", 'Select a2 such that Affects* (25, a2)', "26, 27"),
        # TestCase("assign a2;", 'Select a2 such that Affects* (28, a2)', NONE),
        #
        # # q49
        # TestCase("assign a1;", 'Select a1 such that Affects* (a1, 11)', "11"),
        # TestCase("assign a1;", 'Select a1 such that Affects* (a1, 22)', "19, 21"),
        # TestCase("assign a1;", 'Select a1 such that Affects* (a1, 27)', "25"),
        # TestCase("assign a1;", 'Select a1 such that Affects* (a1, 28)', NONE),
        #
        # # q48
        # TestCase("assign a;", 'Select BOOLEAN such that Affects* (1, 2)', "true"),
        # TestCase("assign a;", 'Select BOOLEAN such that Affects* (1, 3)', "true"),
        # TestCase("assign a;", 'Select BOOLEAN such that Affects* (1, 5)', "true"),
        # TestCase("assign a;", 'Select BOOLEAN such that Affects* (1, 7)', "false"),
        # TestCase("assign a;", 'Select BOOLEAN such that Affects* (11, 11)', "true"),
        # TestCase("assign a;", 'Select BOOLEAN such that Affects* (11, 13)', "true"),
        #
        # # q47
        # TestCase("assign a2;", 'Select a2 such that Affects (1, a2)', "2, 3, 5"),
        # TestCase("assign a2;", 'Select a2 such that Affects (12, a2)', NONE),
        # TestCase("assign a2;", 'Select a2 such that Affects (22, a2)', NONE),
        # TestCase("assign a2;", 'Select a2 such that Affects (28, a2)', NONE),
        #
        # # q46
        # TestCase("assign a1;", 'Select a1 such that Affects (a1, 1)', NONE),
        # TestCase("assign a1;", 'Select a1 such that Affects (a1, 9)', NONE),
        # TestCase("assign a1;", 'Select a1 such that Affects (a1, 22)', "19"),
        # TestCase("assign a1;", 'Select a1 such that Affects (a1, 28)', NONE),
        #
        # # q45
        # TestCase("assign a;", 'Select BOOLEAN such that Affects (1, 2)', "true"),
        # TestCase("assign a;", 'Select BOOLEAN such that Affects (1, 3)', "true"),
        # TestCase("assign a;", 'Select BOOLEAN such that Affects (1, 4)', "false"),
        # TestCase("assign a;", 'Select BOOLEAN such that Affects (2, 5)', "false"),
        # TestCase("assign a;", 'Select BOOLEAN such that Affects (2, 7)', "false"),
        # TestCase("assign a;", 'Select BOOLEAN such that Affects (3, 11)', "false"),
        # TestCase("assign a;", 'Select BOOLEAN such that Affects (3, 12)', "false"),
        # TestCase("assign a;", 'Select BOOLEAN such that Affects (14, 28)', "false"),
        # TestCase("assign a;", 'Select BOOLEAN such that Affects (25, 26)', "true"),
        # TestCase("assign a;", 'Select BOOLEAN such that Affects (25, 27)', "true"),

        # q44
        TestCase("prog_line n2;", 'Select BOOLEAN such that Next* (9, 9)', "false"),
        TestCase("prog_line n2;", 'Select BOOLEAN such that Next* (10, 10)', "true"),
        TestCase("prog_line n2;", 'Select BOOLEAN such that Next* (12, 12)', "true"),
        TestCase("prog_line n2;", 'Select BOOLEAN such that Next* (20, 20)', "true"),

        # q43
        TestCase("prog_line n2;", 'Select BOOLEAN such that Next (1, 2)', "true"),
        TestCase("prog_line n2;", 'Select BOOLEAN such that Next (6, 8)', "false"),
        TestCase("prog_line n2;", 'Select BOOLEAN such that Next (8, 9)', "true"),
        TestCase("prog_line n2;", 'Select BOOLEAN such that Next (10, 11)', "true"),
        TestCase("prog_line n2;", 'Select BOOLEAN such that Next (12, 10)', "true"),
        TestCase("prog_line n2;", 'Select BOOLEAN such that Next (12, 11)', "false"),
        TestCase("prog_line n2;", 'Select BOOLEAN such that Next (13, 14)', "false"),
        TestCase("prog_line n2;", 'Select BOOLEAN such that Next (17, 18)', "false"),
        TestCase("prog_line n2;", 'Select BOOLEAN such that Next (23, 27)', "true"),

        # q42
        # TestCase("if ifstat; constant co;", 'Select BOOLEAN pattern ifstat ("t", _, _) with co.value = 2', "true"),

        # q41
        TestCase("stmt s; constant co;", 'Select s with s.stmt# = co.value', "1, 10, 2, 20, 3"),

        # q40
        # TestCase("assign a;", 'Select a pattern a (_, _"d+1"_)', "12"),

        # q39
        # TestCase("assign a;", 'Select a pattern a (_, _"5+3"_)', NONE),

        # q38
        # TestCase("assign a;", 'Select a pattern a (_, _"3*a"_)', "19"),

        # q37
        # TestCase("assign a;", 'Select a pattern a (_, _"d+3*a"_)', "19"),

        # q36
        # TestCase("assign a;", 'Select a pattern a (_, _"d+3"_)', NONE),

        # q35
        # TestCase("assign a;", 'Select a pattern a (_, _"k+d"_)', NONE),

        # q34
        # TestCase("assign a;", 'Select a pattern a (_, _"d+t"_)', "11, 13, 14"),

        # q33 - SYNTAX ERROR
        # TestCase("assign a;", 'Select a pattern a (_, _"t*a+d"_)', "Syntax error"),

        # q32
        # TestCase("assign a;", 'Select a pattern a (_, _"d+k*b"_)', NONE),

        # q31
        # TestCase("assign a;", 'Select a pattern a (_, _"d*5+3"_)', NONE),

        # q30
        # TestCase("assign a;", 'Select a pattern a (_, _"d+1"_)', "12"),

        # q29
        # TestCase("while w; assign a;", 'Select a pattern a ("a", _) such that Follows (w, a)', "13"),

        # q28
        # TestCase("assign a;", 'Select a pattern a ("t", "3*a")', NONE),

        # q27
        # TestCase("assign a;", 'Select a pattern a ("d", _)', "11, 22, 25, 3"),

        # q26
        TestCase("procedure p;", 'Select p such that Calls* ("Circle", p) and Modifies (p, "c")',
                 "Rectangle, Triangle"),
    ]


def get_database():
    test_program_code = """
    procedure Circle {
    t = 1;
    a = t + 10;
    d = t * (a + 2);
    call Triangle;
    b = t + a;
    call Hexagon;
    b = t + a;
    if t then {
        k = a - d;
        while c {
            d = d + t;
            c = d + 1; }
        a = d + t; }
    else {
        a = d + t;
        call Hexagon;
        c = c - 1; }
call Rectangle; }

procedure Rectangle {
    while c  {
        t = d + 3 * a + c;
        call Triangle;
        c = c + 20; }
    d = t; }

procedure Triangle {
    while d {
        if t then {
            d = t + 2; }
        else {
            a = t * a + d + k * b; }}
    c = t + k + d; }

procedure Hexagon {
    t = a + t; }
    """
    all_cases = get_krzysztof_cases() + get_iza_cases()
    test_program = TestProgram(test_program_code, all_cases)
    return test_program
