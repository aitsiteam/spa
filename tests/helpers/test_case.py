class TestCase:
    calls = False
    calls_asterisk = False
    parent = False
    parent_asterisk = False
    uses = False
    modifies = False
    follows = False
    follows_asterisk = False
    next = False
    next_asterisk = False

    def __init__(self, definitions, query, answer):
        """
        Creates a test case for PQL or PKB
        :param definitions: Definitions of used elements, e.g. stmt s, s1;
        :param query: Self-explanatory, e.g. Select s such that Parent (s, n)
        :param answer: Self-explanatory, e.g. 8
        """
        self.definitions = definitions
        self.query = query
        self.answer = answer
        self._check_query_usages()

    # TODO: add the rest of methods to set these up
    def _check_query_usages(self):
        """
        Checks if there are the test case uses certain elements such as calls, follows, etc
        :return:
        """
        self._has_calls()
        self._has_calls_asterisk()
        self._has_follows()
        self._has_follows_asterisk()
        self._has_modifies()
        self._has_next()
        self._has_next_asterisk()
        self._has_parent()
        self._has_parent_asterisk()
        self._has_uses()

    def _has_calls(self):
        if "Calls" in self.query and 'Calls*' not in self.query:
            self.calls = True

    def _has_calls_asterisk(self):
        if "Calls*" in self.query:
            self.calls_asterisk = True

    def _has_parent(self):
        if "Parent" in self.query and 'Parent*' not in self.query:
            self.parent = True

    def _has_parent_asterisk(self):
        if "Parent*" in self.query:
            self.parent_asterisk = True

    def _has_uses(self):
        if "Uses" in self.query:
            self.uses = True

    def _has_modifies(self):
        if "Modifies" in self.query:
            self.modifies = True

    def _has_follows(self):
        if "Follows" in self.query and 'Follows*' not in self.query:
            self.follows = True

    def _has_follows_asterisk(self):
        if "Follows*" in self.query:
            self.follows_asterisk = True

    def _has_next(self):
        if "Next" in self.query and 'Next*' not in self.query:
            self.next = True

    def _has_next_asterisk(self):
        if "Next*" in self.query:
            self.next_asterisk = True
