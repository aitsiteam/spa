# ---------------- RESULT TREE ------------------------
from PQL.keywords import *

NONE = "none"


class ResultTree:
    result = []
    no_results = False
    result_tree_row = {}

    def __init__(self, params, pkb, searchable_param):
        self.pkb = pkb
        self.searchable_param = searchable_param

        self.result = self.init_result_tree()
        self.result_tree_row = self.init_result_tree_row(params)


    def init_result_tree(self) -> list:
        return list()

    def init_result_tree_row(self, params):
        row = {param.param_name: "" for param in params}
        row['deleted'] = False
        return row

    def get_all_values_by_param(self, param):
        return set([item[param] for item in self.result if item[param] != '' and item['deleted'] is False])

    def get_pairs_by_params(self, param, param2):
        return set((item[param], item[param2]) for item in self.result if item['deleted'] is False)

    def get_new_row(self, param, value):
        new_row = dict(self.result_tree_row)
        new_row[param] = value
        return new_row

    def modify_values(self, param, values):
        available_values = self.get_all_values_by_param(param)

        # pierwsze wprowadzone dane do self.result
        if len(self.result) == 0:
            for i in range(0, len(values)):
                val = values[i]
                x = self.get_new_row(param, val)
                self.result.append(x)
            # motyw gdy juz sa dodane wyniki z tym parametrem
        elif len(available_values) != 0:
            for item in self.result:
                if item[param] not in values:
                    item['deleted'] = True

        else:
            new_result = list(self.result)
            for i in range(0, len(self.result)):
                item = self.result[i]
                for value in values:
                    if item[param] == '':
                        new_result[i][param] = value
                    else:
                        # nowy wiersz- skopiowane poprzednie dane i dostawiona nowa wartosc parametru
                        new_row = dict(item)
                        new_row[param] = value
                        new_result.append(new_row)
            self.result = new_result

    def modify_values_by_pairs(self, single_param_name, connected_param_name, pairs):

        # 1 gdy puste drzewo
        if len(self.result) == 0:
            for value, value2 in pairs:
                row_def = dict(self.result_tree_row)

                row_def[single_param_name] = value
                row_def[connected_param_name] = value2

                self.result.append(row_def)
            return

        available_pairs = self.get_pairs_by_params(single_param_name, connected_param_name)

        # z seta nie da sie wyjac pierwszego argumentu, wiec uzywamy next
        # potrzebujemy tego aby zobaczyc jakie wyniki dostalismy z availble_params
        # ('',<nazwa>), czy (<nazwa>, <nazwa2>), inaczej obslugujemy
        available_pair = next(iter(available_pairs))

        # 2 usuwanie jesli nie pasuja do juz obecnych w drzewie
        if available_pair[0] == '' and available_pair[1]:
            values = [value[1] for value in pairs]

            for result_row in self.result:
                if result_row[connected_param_name] not in values:
                    result_row['deleted'] = True

            self.__add_or_modify_by_pairs(single_param_name, connected_param_name, pairs)

        elif available_pair[0] and available_pair[1] == '':
            values = [value[0] for value in pairs]

            for result_row in self.result:
                if result_row[single_param_name] not in values:
                    result_row['deleted'] = True

            self.__add_or_modify_by_pairs(single_param_name, connected_param_name, pairs)

        elif available_pair[0] and available_pair[1]:
            # poniewaz przy wywolywaniu metod z api bralismy tylko pary dostepne w drzewie rozwiazan
            # wystarczy ze tu wywalimy z drzewa te pary ktore nie znalazly sie w 'pairs'

            for result_row in self.result:
                pair_row = (result_row[single_param_name], result_row[connected_param_name])

                if pair_row not in pairs:
                    result_row['deleted'] = True

    def __add_or_modify_by_pairs(self, single_param_name, connected_param_name, pairs):
        new_result = list(self.result)
        for i in range(0, len(self.result)):
            result_row = new_result[i]
            if result_row['deleted'] is True:
                continue

            for value, value2 in pairs:
                if result_row[single_param_name] == '' and result_row[connected_param_name] == value2:
                    new_result[i][single_param_name] = value

                elif result_row[single_param_name] == value and result_row[connected_param_name] == '':

                    new_result[i][connected_param_name] = value2
                elif result_row[single_param_name] == value and result_row[connected_param_name] != '':
                    # nowy wiersz- skopiowane poprzednie dane i dostawiona nowa wartosc parametru
                    new_row = dict(result_row)
                    new_row[connected_param_name] = value2
                    new_result.append(new_row)

                elif result_row[single_param_name] != '' and result_row[connected_param_name] == value2:
                    # nowy wiersz- skopiowane poprzednie dane i dostawiona nowa wartosc parametru
                    new_row = dict(result_row)
                    new_row[single_param_name] = value
                    new_result.append(new_row)

        self.result = new_result

    def get_result_tree(self):
        return self.result

    def set_no_results(self):
        self.no_results = True

    # Ostateczny wynik
    def get_result_by_param(self, param):

        #boolean option
        if param is None:
            return "false" if self.no_results else "true"
        elif self.no_results:
            return NONE
        elif isinstance(self.searchable_param, list):

            results= set()
            for item in self.result:
                valid = item['deleted'] is False
                current_result = list()
                for param in self.searchable_param:
                    if item[param.param_name] != '':

                        current_result.append(item[param.param_name])
                    else:
                        valid = False
                if valid:
                    results.add(tuple(current_result))
            if len(results) == 0:
                return NONE
            return self.__convert_tuples(results)
        else:

            results = set([item[param.param_name] for item in self.result if
                           item[param.param_name] != '' and item['deleted'] is False])

            # filtrowanie jesli  typ parametru to WHILE, IF, ASSIGN, cALL
            if param.param_type in (WHILE, IF, ASSIGN, CALL):
                results = self.__filter_stmt(results)

            if len(results) == 0:
                return NONE
            return self.__convert(results)

    def __filter_stmt(self, results):
        all_values_by_type = self.__get_default_list_by_type(
             self.searchable_param if self.searchable_param is None else self.searchable_param.param_type)
        return set(all_values_by_type) & results

    def __convert(self, results):
        result = sorted(results)
        s = [str(i) for i in result]

        # Join list items using join()
        result = ", ".join(s)

        return result

    def __convert_tuples(self, results):
        result = sorted(results)
        s = list()
        for res in result:
            s.append(' '.join(str(i) for i in res))

        # Join list items using join()
        result = ','.join(s)

        return result

    def __get_default_list_by_type(self, type) -> tuple:

        if type == ASSIGN:
            return self.pkb.StmtTable.get_all_assign()
        elif type == WHILE:
            return self.pkb.StmtTable.get_all_whiles()
        elif type == IF:
            return self.pkb.StmtTable.get_all_ifs()
        elif type == CALL:
            return self.pkb.StmtTable.get_all_calls()
        elif type == STMT:
            return self.pkb.StmtTable.get_all_stmts()
        elif type == PROCEDURE:
            return self.pkb.ProcTable.get_all_procedures()
        elif type == PROG_LINE:
            return self.pkb.StmtTable.get_all_stmts()
        elif type == VARIABLE:
            return self.pkb.VarTable.get_all_variables()
        elif type == CONSTANT:
            pass
            # todo
        else:
            return tuple()