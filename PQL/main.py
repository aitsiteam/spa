import sys
from PQL.pql_parser import PqlParser
import os


def main():
    # dir_name = os.path.dirname(__file__)
    # dir = dir_name.split(os.path.basename(dir_name))[0]
    # file_name = os.path.join(dir, 'PKB/Parser/SIMPLE Samples/programsource.txt')
    # p = PqlParser(file_name)
    #
    #s1 = 'stmt s1, s2;'
    #s2 = 'Select BOOLEAN such that Parent* (s1, s2) with s1.stmt# = 136 and s2.stmt# = 149'
    #print(p.run(s1, s2))


    # TESTY EWELI
    # test_file = os.path.join(dir, 'Tests/Calls.txt')
    # f = open(test_file, 'r')

    """
    number_of_lines = 5
    print('start')
    failure_counter = 0
    success_counter = 0
    with open(test_file, 'r') as filehandle:
        filecontent = filehandle.readlines()
        for i in range(0, len(filecontent), 4):
            query1 = filecontent[i].strip()
            query2 = filecontent[i + 1].strip()
            test_answer = filecontent[i + 2].strip()

            pql_answer = p.run(query1, query2)
            if test_answer != pql_answer:
                print('!!! FAILURE !!!!')
                print('QUERY: ', query1, query2)
                print('TEST_ANSWER: ', test_answer)
                print('PQL_ANSWER: ', pql_answer)
                failure_counter += 1
            else:
                success_counter += 1
            print("----------------")
        print("SUCCESS - ", success_counter)
        print("FAILURE - ", failure_counter)
    # KONIEC TESTOW EWELI
    """

    # ZWYKLY INPUT
    # while True:
    #     params_line = input()
    #     query_line = input()
    #     print(p.run(params_line, query_line))

    # PIPE TESTER
    if len(sys.argv) < 2 or not sys.argv[1]:
        sys.stdout.write('No source code file.')
        sys.stdout.flush()
        return

    file_name = sys.argv[1]
    p = PqlParser(file_name)
    sys.stdout.write('Ready\n')
    sys.stdout.flush()

    while True:
        params_line = sys.stdin.readline().strip()
        query_line = sys.stdin.readline().strip()
        sys.stdout.write(p.run(params_line, query_line) + '\n')
        sys.stdout.flush()


if __name__ == "__main__":
    main()
