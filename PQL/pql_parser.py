from PQL.result_tree import ResultTree, NONE
from PQL.validator import is_relation, is_param_a_keyword, is_param_name

from PQL.validator import is_relation

from PQL.keywords import *

from PKB.Parser.parser import parse
from PKB.API.api import API


class Param:
    def __init__(self, param_type, param_name, is_searchable):
        self.param_type = param_type
        self.param_name = param_name
        self.is_searchable = is_searchable  # czy parametr jest szukanym

    def __str__(self):
        if self.is_searchable:
            return "TYPE: " + self.param_type + ', NAME: ' + self.param_name + " - SEARCHABLE "
        else:
            return "TYPE: " + self.param_type + ', NAME: ' + self.param_name


class Arg:
    def __init__(self, arg_type, arg_value):
        self.arg_type = arg_type
        self.arg_value = arg_value

    def __str__(self):
        return "TYPE: " + self.arg_type + ', VALUE: ' + self.arg_value


class QueryException(Exception):
    pass


class PqlParser:
    params = []  # lista parametrow - linia wejścia nr 1
    params_names = []  # tablica pomocnicza z samymi nazwami
    relations_tree = {}  # drzewko relacji
    searchable_param = None
    final_result = NONE

    def __init__(self, file):
        self.pkb = parse(file)
        self.api = API(self.pkb)

    def run(self, params_line, query_line):
        params_line = params_line.strip('\n')
        query_line = query_line.strip('\n')
        # try:
        response = self.__parse_params(params_line)
        # zwracanie błędu w parametrach, jesli sie pojawil
        if response is not None:
            return response

        query_elements = self.__parse_query(query_line)
        self.__init_relations_tree()
        response = self.__prepare_execution_tree(query_elements)

        # wyswietlanie paramsow - pomocnicze
        # for param in self.params:

        # wyswietlanie drzewka relacji
        # print("\nRELATION TREE: ")
        # for leaf, args in self.relations_tree.items():
        #     print(leaf, ':')
        #     for arg in args:
        #         print('\t', '->')
        #         for r in range(0, len(arg)):
        #             print(len(arg))
        #             print(arg)
        #             print('\t\t', arg[r])

        # zwracanie błędu w zapytaniu, jesli sie pojawil
        if response is not None:
            return response

        self.__execute_query()

        return self.final_result
        # except:
        #     return "#ERROR"

    def __parse_params(self, params_line):
        """ Parsuje parametry, waliduje i zapisuje w tablicy params """

        if params_line.strip() == '' or None:
            return
        params_elements = params_line.split(';')
        for param in params_elements:

            if param == '':
                continue

            # usuwanie nadmiarowych spacji
            param = " ".join(param.split())

            # podzial stringa na czesci (po spacjach)
            param_details = param.split(' ')

            if len(param_details) < 2:
                return ERRORS.INVALID_PARAM_LENGTH
            # walidacja - pierwsze slowo kluczowe, drugie nie
            if not is_param_a_keyword(param_details[0]):
                return ERRORS.INVALID_PARAM_WRONG_DETAILS

            param_type = param_details[0]
            for i in range(1, len(param_details)):
                p = param_details[i].split(',')[0]
                par = Param(param_type, p, False)
                self.params.append(par)
                self.params_names.append(p)

    def __parse_query(self, query):
        """ walidacja i parsowanie linii zapytania"""
        query = " ".join(query.split())

        # usuwanie nadmiarowych spacji
        query_elements = " ".join(query.split())

        # Przy ) tu tylko z lewej spacje usuwamy
        # '/ + - * ' <- pattern
        query_elements = query_elements.replace(' (', '('). \
            replace('( ', '(').replace(' )', ')'). \
            replace(' ,', ',').replace(', ', ','). \
            replace(' =', '=').replace('= ', '='). \
            replace(' *', '*').replace('* ', '*'). \
            replace(' +', '+').replace('+ ', '+'). \
            replace(' -', '-').replace('- ', '-'). \
            replace(' /', '/').replace('/ ', '/'). \
            replace('< ', '<').replace(' >', '>')

        query_elements = query_elements.split()

        # laczymy such that
        tmp = query_elements
        for i in range(0, len(query_elements) - 2):
            if query_elements[i] == 'such':
                tmp[i:i + 2] = [' '.join(tmp[i:i + 2])]

        query_elements = tmp

        return query_elements

    def __init_relations_tree(self):
        for relation in RELATION_KEYWORDS + [WITH, PATTERN]:
            self.relations_tree[relation] = []

    def find_replace_leaf(self, variable, value):
        new_relation_tree = self.relations_tree
        exist = False  # gdy nie bedzie w drzewku dodajemy jako dodatkowa relacja

        for relation, params in self.relations_tree.items():

            if relation in RELATION_KEYWORDS:
                new_params = []
                for param in params:
                    par1_value = param[0].arg_value
                    par1_type = param[0].arg_type
                    par2_value = param[1].arg_value
                    par2_type = param[1].arg_type
                    if param[0].arg_value == variable:
                        exist = True
                        par1_value = value

                        if self.__is_integer(par1_value):
                            par1_type = INTEGER
                        else:
                            par1_type = STRING
                    if param[1].arg_value == variable:
                        exist = True

                        par2_value = value
                        if self.__is_integer(par2_value):
                            par2_type = INTEGER
                        else:
                            par2_type = STRING

                    node = [Arg(par1_type, par1_value), Arg(par2_type, par2_value)]

                    new_params.append(node)
            else:
                new_params = params
            new_relation_tree[relation] = new_params

        self.relations_tree = new_relation_tree

        if not exist:
            self.relations_tree['with'].append({'variable': variable, 'value': value})

    def __add_with_statement_to_tree(self, with_statement):
        # here we must find this variables on tree and if are not available on the tree so add that with 'with' key

        # wyłapanie prog_line
        if len(with_statement.split('.')) == 1:

            param = with_statement.split('=')[0]
            attr = 'prog_line'

        else:
            param = with_statement.split('.')[0]
            attr = with_statement.split('.')[1].split('=')[0]

        value = with_statement.split('=')

        if param in self.params_names and len(value) == 2:
            if self.__validate_with_statement_params(param, attr, value[1]):
                self.find_replace_leaf(param, value[1])
            else:
                return ERRORS.INVALID_QUERY_WRONG_RELATION_ARGS

        else:
            return ERRORS.INVALID_QUERY_WRONG_RELATION_ARGS

    def __validate_with_statement_params(self, param, attr, value):

        # sprawdzenie, czy atrybut with jest poprawny
        if attr not in WITH_ATTRIBUTES_KEYWORDS:
            return False

        # sprawdzenie poprawności prog_line
        if attr == PROG_LINE:
            # param musi być prog_line
            if self.__get_param_type_by_name(param) != PROG_LINE:
                return False
            # value musi być liczba
            if not value.isnumeric():
                return False

        # sprawdzenie poprawności procName
        if attr == PROC_NAME:
            # param musi być procedura
            if self.__get_param_type_by_name(param) != PROCEDURE:
                return False
            # wartość nie może byc liczba
            if value.isnumeric():
                return False
            # string musi być w cudzyslowie
        # if value[0] != '"' or value[len(value) - 1] != '"':
        #    return False

        # sprawdzenie poprawności varName
        elif attr == VAR_NAME:
            # param musi być typu variable
            if self.__get_param_type_by_name(param) != VARIABLE:
                return False
            # wartość nie może byc liczba
            if value.isnumeric():
                return False
            # string musi być w cudzyslowie
            # if value[0] != '"' or value[len(value) - 1] != '"':
            #     return False

        # sprawdzenie poprawności value
        elif attr == VALUE:
            # param musi być typu constant
            if self.__get_param_type_by_name(param) != CONSTANT:
                return False
            # wartość nie może byc stringiem
            if not value.isnumeric():
                return False

        # sprawdzenie poprawności stmt_number
        elif attr == STMT_NUMBER:
            # stmt musi być jednym z rodzajów statement
            if self.__get_param_type_by_name(param) not in STMT_TYPES:
                return False
            # value może byc liczba
            if value.isnumeric():
                return True
            else:
                # możemy odwołac sie do wcześniej zadekalrowanej const
                const_name = value.split('.value')[0]
                if not self.__get_param_type_by_name(const_name) == CONSTANT:
                    return False

        return True

    def __add_pattern_statement_to_tree(self, pattern_statement):
        param = pattern_statement.split('(')[0]
        pattern_statement = pattern_statement.split('(')
        args = pattern_statement[1].replace(')', '').split(',')

        # if param in self.params_names and len(args) == 2:
        if self.__validate_pattern_statement_params(param, args):
            # Select a pattern a (“t”, “3 * a”)
            self.relations_tree['pattern'].append(
                [Arg(self.__check_type(param), param),
                 Arg(PATTERN, args[0]),
                 Arg(PATTERN, str(args[1]))
                 ])
        else:
            return ERRORS.INVALID_QUERY_WRONG_RELATION_ARGS

    def __validate_pattern_statement_params(self, param, args):
        # sprawdzanie typu parametru
        param_type = self.__get_param_type_by_name(param)
        if param_type not in PATTERN_PARAMS:
            return False

        # wydobycie rodzajów expression
        expr_counter = 0
        sub_expr_counter = 0
        floor_counter = 0

        for arg in args:
            exp_type = self.__find_pattern_expr_type(arg)
            if exp_type == EXPR:
                expr_counter += 1
            elif exp_type == SUB_EXPR:
                sub_expr_counter += 1
            elif exp_type == FLOOR:
                floor_counter += 1

        # pattern if musi miec 3 argumenty
        if param_type == IF:
            if len(args) != 3:
                return False
            # tutaj sprawdzac wyrazenia w ifie
        else:
            # sprawdzenie expr w standardowych patternach
            if expr_counter == 2 or (expr_counter == 1 and floor_counter == 1) or (
                    sub_expr_counter == 1 and floor_counter == 1):
                return True
            else:
                return False
        return True

    def __find_pattern_expr_type(self, arg):
        if arg[0] == '"' and arg[len(arg) - 1] == '"':
            return EXPR
        elif arg == '_':
            return FLOOR
        elif len(arg) > 4 and arg[0] == '_' and arg[1] == '"' and arg[len(arg) - 1] == '_' and arg[len(arg) - 2] == '"':
            return SUB_EXPR
        return None

    def __prepare_execution_tree(self, query):
        """ walidacja i zapis relacji które mają się wykonać -  tablica: relations_list"""

        # sprawdzamy 2 pierwsze slowa
        if query[0] != SELECT:
            return ERRORS.INVALID_QUERY_SELECT

        # Dorzucilam tez oddzielone such that, bo sa to oddzielne slowa, a w ch mamy polaczone
        if query[1] in GRAMMAR_KEYWORDS + RELATION_KEYWORDS + QUERY_KEYWORDS:
            return ERRORS.INVALID_QUERY_WRONG_PARAM_NAME

        # sprawdzamy, czy parametr szukany w select byl zadeklarowany - zrobic z tego ladna funkcje ;)
        illegal_param_in_query = True
        is_tuple = False
        param_attr_to_check = ''
        if query[1] == BOOLEAN:
            illegal_param_in_query = False
            self.searchable_param = None

        #     zapytania typu select p.procName,select v.varName
        if query[1].__contains__('.'):
            param_name_to_check = query[1].split('.')[0]
            param_attr_to_check = query[1].split('.')[1]
        #     tuple
        elif query[1].startswith('<') and query[1].endswith('>'):
            is_tuple = True
            tuple_elements = query[1].replace('<', '').replace('>','').split(',')
        else:
            param_name_to_check = query[1]

        if not is_tuple:
            for param in self.params:

                if param.param_name == param_name_to_check:
                    if param_attr_to_check is '' or param_attr_to_check in [PROC_NAME, VAR_NAME, STMT_NUMBER, VALUE]:
                        illegal_param_in_query = False
                        param.is_searchable = True
                        self.searchable_param = param
        else:
            new_tuple_params= []
            param_not_found = False
            for param in self.params:
                if param.param_name in tuple_elements:
                    param.is_searchable = True
                    new_tuple_params.append(param)
                # else:
                #     param_not_found = True
            if not param_not_found:
                illegal_param_in_query = False
                self.searchable_param = new_tuple_params

        if illegal_param_in_query:
            return ERRORS.INVALID_QUERY_WRONG_PARAM_NAME

        # krotkie zapytania
        if len(query) < 3:
            return

        prev = query[2]
        is_previous_query_key = prev in NEXT_KEYWORDS[SELECT]
        is_in_with = False

        # sprawdzanie czy 3 slowo jest kluczowe
        if not is_previous_query_key:
            return ERRORS.INVALID_QUERY_WRONG_KEYWORD

        for i in range(3, len(query), 2):
            # jesli poprzedni to query key
            # jesli mamy such that albo with, to musi wystapic po nim relacja
            prev = query[i - 1]
            is_previous_query_key = prev in QUERY_KEYWORDS_MULTIPLE, WITH_ATTRIBUTES_KEYWORDS

            if is_previous_query_key and prev in [SUCH_THAT, AND]:
                if is_relation(query[i]):
                    is_in_with = False
                    response = self.__add_relation_to_tree(query[i])
                    if response is not None:
                        return response
                elif is_in_with:
                    response = self.__add_with_statement_to_tree(query[i])

                else:
                    return ERRORS.INVALID_QUERY_WRONG_RELATION_WORD

            # tu dodac pozniej sprawdzanie patterna i dalej with
            elif is_previous_query_key and prev in [PATTERN]:
                is_in_with = False
                response = self.__add_pattern_statement_to_tree(query[i])
                if response:
                    return response
            elif is_previous_query_key and prev in [WITH]:
                is_in_with = True
                response = self.__add_with_statement_to_tree(query[i])
                if response:
                    return response

    def __check_type(self, value):
        if value == '_':
            type = DEFAULT
        elif self.__is_integer(value):
            type = INTEGER
        elif self.__is_string(value):
            type = STRING
        else:
            type = self.__get_param_type_by_name(value)

        return type

    def __add_relation_to_tree(self, relation):

        relation = relation.split('(')
        relation_name = relation[0]
        relations_args = relation[1].replace(')', '').split(',')

        current_branch = self.relations_tree[relation_name]

        par_1_value = relations_args[0]
        par_2_value = relations_args[1]

        # dopasowanie typów do parametrow relacji
        par_1_type = self.__check_type(par_1_value)
        par_2_type = self.__check_type(par_2_value)

        if par_1_type is None or par_2_type is None:
            return ERRORS.UNDECLARED_RELATION_PARAM

        new_node = [Arg(par_1_type, par_1_value), Arg(par_2_type, par_2_value)]
        current_branch.append(new_node)
        self.relations_tree[relation_name] = current_branch

    def __get_param_type_by_name(self, param):
        for p in self.params:
            if p.param_name == param:
                return p.param_type
        return None

    def __basic_select_search(self):

        if isinstance(self.searchable_param, list):
            for item in self.searchable_param:
                self.result_tree.modify_values(item.param_name,
                                               self.__get_default_list_by_type(item.param_type))

        elif self.searchable_param is not None:
            self.result_tree.modify_values(self.searchable_param.param_name,
                                           self.__get_default_list_by_type(self.searchable_param.param_type))

    def __execute_query(self):
        # self.result_tree = ResultTree(self.params, self.__get_default_list_by_type(
        #     self.searchable_param if self.searchable_param is None else self.searchable_param.param_type))

        self.result_tree = ResultTree(self.params, self.pkb, self.searchable_param)

        self.__basic_select_search()
        # wysylanie zapytan do API

        for rel, items in self.relations_tree.items():
            if len(items) > 0:
                # loop for relation items
                for i in range(0, len(items)):

                    if rel is CALLS:
                        self.__execute_call_relation(items[i], False)
                    elif rel is CALLS_S:
                        self.__execute_call_relation(items[i], True)
                    elif rel is NEXT:
                        self.__execute_next_relation(items[i], False)
                    elif rel is NEXT_S:
                        self.__execute_next_relation(items[i], True)
                    elif rel is PARENT:
                        self.__execute_parent_relation(items[i], False)
                    elif rel is PARENT_S:
                        self.__execute_parent_relation(items[i], True)
                    elif rel is FOLLOWS:
                        self.__execute_follows_relation(items[i], False)

                    elif rel is FOLLOWS_S:
                        self.__execute_follows_relation(items[i], True)

                    elif rel is MODIFIES:
                        self.__execute_modifies_relation(items[i])

                    elif rel is USES:
                        self.__execute_uses_relation(items[i])

                    elif rel is WITH:
                        self.__execute_with_extra_relation(items[i])

                    # pattern

        self.final_result = self.result_tree.get_result_by_param(self.searchable_param)

    def __is_string(self, arg):
        arg_val = arg
        if len(arg) <= 2:
            return False
        if arg[0] == '"' and arg[len(arg) - 1] == '"':
            return True
        return False

    def __is_integer(self, arg):
        if arg.isnumeric():
            return True
        return False

    def __format_argument(self, arg: Arg):
        return int(arg.arg_value) if arg.arg_type == INTEGER else arg.arg_value.replace('"', '')

    def __get_default_list_by_type(self, type) -> tuple:
        if type == ASSIGN:
            return self.pkb.StmtTable.get_all_assign()
        elif type == WHILE:
            return self.pkb.StmtTable.get_all_whiles()
        elif type == IF:
            return self.pkb.StmtTable.get_all_ifs()
        elif type == CALL:
            return self.pkb.StmtTable.get_all_calls()
        elif type == STMT:
            return self.pkb.StmtTable.get_all_stmts()
        elif type == PROCEDURE:
            return self.pkb.ProcTable.get_all_procedures()
        elif type == PROG_LINE:
            return self.pkb.StmtTable.get_all_stmts()
        elif type == VARIABLE:
            return self.pkb.VarTable.get_all_variables()
        elif type == CONSTANT:
            pass
            # todo
        else:
            return tuple()

    def __execute_default(self, arg1, arg2, get_from, get_to, is_relation):
        # Musimy miec caly wynik calls dla par, dopiero majac mozliwe pary przechodzimy do zmieniania result tree
        values = set()  # set zeby miec unikalne pary

        # Relacja(_, p)
        if arg1.arg_type == DEFAULT and arg2.arg_type != DEFAULT:
            arg2_value = self.__format_argument(arg2)
            arg2_values = self.result_tree.get_all_values_by_param(arg2_value)
            default_list = self.__get_default_list_by_type(arg2.arg_type)

            # dany parametr nie istnieje w drzewie rozwiazan
            if len(arg2_values) == 0:
                for proc in default_list:
                    if len(get_to(proc)) > 0:
                        values.add(proc)

            # dany parametr istnieje w drzewie rozwiazan
            else:
                for proc in arg2_values:
                    if len(get_to(proc)) > 0:
                        values.add(proc)

            if len(values) > 0:
                self.result_tree.modify_values(arg2_value, values)
            else:
                self.result_tree.set_no_results()
        # Relacja(p,_)
        elif arg1.arg_type != DEFAULT and arg2.arg_type == DEFAULT:
            arg1_value = self.__format_argument(arg1)
            arg1_values = self.result_tree.get_all_values_by_param(arg1_value)
            default_list = self.__get_default_list_by_type(arg1.arg_type)

            # dany parametr nie istnieje w drzewie rozwiazan
            if len(arg1_values) == 0:
                for proc in default_list:
                    if len(get_from(proc)) > 0:
                        values.add(proc)

            # dany parametr istnieje w drzewie rozwiazan
            else:
                for proc in arg1_values:
                    if len(get_from(proc)) > 0:
                        values.add(proc)

            if len(values) > 0:
                self.result_tree.modify_values(arg1_value, values)
            else:
                self.result_tree.set_no_results()
        # _,_ olac
        else:
            pass

    def __execute_relation_double_unknown(self, arg1, arg2, get_from, get_to, is_relation):
        default_list_arg1 = self.__get_default_list_by_type(arg1.arg_type)
        default_list_arg2 = self.__get_default_list_by_type(arg2.arg_type)
        arg1 = self.__format_argument(arg1)
        arg2 = self.__format_argument(arg2)
        arg1_values = self.result_tree.get_all_values_by_param(arg1)
        arg2_values = self.result_tree.get_all_values_by_param(arg2)

        # Musimy miec caly wynik calls dla par, dopiero majac mozliwe pary przechodzimy do zmieniania result tree
        pairs = set()  # set zeby miec unikalne pary

        # BARDZO wAZNE zeby pozniej dzialala prawidlowa funkcja w result_tree
        if len(arg1_values) == 0 and len(arg2_values) == 0:
            for proc in default_list_arg1:
                for proc2 in default_list_arg2:
                    if is_relation(proc, proc2):
                        pairs.add((proc, proc2))
        elif len(arg1_values) == 0 and len(arg2_values) != 0:
            for proc in default_list_arg1:
                for proc2 in arg2_values:
                    if is_relation(proc, proc2):
                        pairs.add((proc, proc2))

        elif len(arg1_values) != 0 and len(arg2_values) == 0:
            for proc in arg1_values:
                for proc2 in default_list_arg2:
                    if is_relation(proc, proc2):
                        pairs.add((proc, proc2))

        else:
            existing_pairs = self.result_tree.get_pairs_by_params(arg1, arg2)

            for value, value2 in existing_pairs:
                is_connection = is_relation(value, value2)
                if is_connection:
                    pairs.add((value, value2))

        if len(pairs) > 0:
            self.result_tree.modify_values_by_pairs(arg1, arg2, pairs)
        else:
            self.result_tree.set_no_results()

    # tworzymy wynik relacji i edytujemy result tree
    def __execute_call_relation(self, items, with_star):
        arg1 = self.__format_argument(items[0])
        arg2 = self.__format_argument(items[1])
        # Calls("A", "B")
        # print(items[0])
        # print(items[1])
        if items[0].arg_type in [STRING, INTEGER] and items[1].arg_type in [STRING, INTEGER]:

            is_connection = self.pkb.Calls.is_calls_t(arg1, arg2) if with_star else self.pkb.Calls.is_calls(arg1, arg2)
            if not is_connection:
                self.result_tree.set_no_results()

        # Calls("A", p) lub Calls("A", _) gdzie q jest parametrem z pierwszej linii wejsciowej
        elif items[0].arg_type == STRING and items[1].arg_type in (PROCEDURE, DEFAULT):
            values = self.pkb.Calls.get_called_from_t(arg1) if with_star else self.pkb.Calls.get_called_from(arg1)

            if values:
                if items[1].arg_type == PROCEDURE:
                    self.result_tree.modify_values(arg2, values)
            else:
                self.result_tree.set_no_results()

        # Calls( p, "B") lub Calls(_,"B") gdzie p jest parametrem z pierwszej linii wejsciowej
        elif items[0].arg_type in (PROCEDURE, DEFAULT) and items[1].arg_type == STRING:

            values = self.pkb.Calls.get_calls_t(arg2) if with_star else self.pkb.Calls.get_calls(arg2)
            if values:
                if items[0].arg_type == PROCEDURE:
                    self.result_tree.modify_values(arg1, values)
            else:
                self.result_tree.set_no_results()
        # zmienna + '_'
        elif items[0].arg_type == DEFAULT or items[1].arg_type == DEFAULT:
            self.__execute_default(items[0], items[1],
                                   self.pkb.Calls.get_called_from_t if with_star else self.pkb.Calls.get_called_from,
                                   self.pkb.Calls.get_calls_t if with_star else self.pkb.Calls.get_calls,
                                   self.pkb.Calls.is_calls_t if with_star else self.pkb.Calls.is_calls)
        # Calls( p, q) gdzie p,q sa parametrami z pierwszej linii wejsciowej
        else:
            self.__execute_relation_double_unknown(items[0], items[1],
                                                   self.pkb.Calls.get_called_from_t if with_star else self.pkb.Calls.get_called_from,
                                                   self.pkb.Calls.get_calls_t if with_star else self.pkb.Calls.get_calls,
                                                   self.pkb.Calls.is_calls_t if with_star else self.pkb.Calls.is_calls)

    def __execute_next_relation(self, items, with_star):
        arg1 = self.__format_argument(items[0])
        arg2 = self.__format_argument(items[1])

        available_types = (STMT, WHILE, ASSIGN, CALL, IF, PROG_LINE)
        # Parent(1,5)
        if items[0].arg_type == INTEGER and items[1].arg_type == INTEGER:
            is_connection = self.pkb.Next.is_next_t(arg1, arg2) if with_star else self.pkb.Next.is_next(
                arg1, arg2)
            if not is_connection:
                self.result_tree.set_no_results()

        # Parent(6, p) gdzie p jest parametrem z pierwszej linii wejsciowej
        elif items[0].arg_type == INTEGER and (items[1].arg_type in available_types or items[1].arg_type == DEFAULT):
            values = self.pkb.Next.get_stmts_after_t(arg1) if with_star else self.pkb.Next.get_stmts_after(arg1)
            if values:
                if items[1].arg_type in available_types:
                    self.result_tree.modify_values(arg2, values)
            else:
                self.result_tree.set_no_results()

        # Parent( p, "B") gdzie p jest parametrem z pierwszej linii wejsciowej
        elif (items[0].arg_type in available_types or items[0].arg_type == DEFAULT) and items[1].arg_type == INTEGER:
            values = self.pkb.Next.get_stmts_before_t(arg2) if with_star else self.pkb.Next.get_stmts_before(arg2)
            if values:
                if items[0].arg_type in available_types:
                    self.result_tree.modify_values(arg1, values)
            else:
                self.result_tree.set_no_results()
                # zmienna + '_'

        elif items[0].arg_type == DEFAULT or items[1].arg_type == DEFAULT:
            self.__execute_default(items[0], items[1],
                                   self.pkb.Next.get_stmts_after_t if with_star else self.pkb.Next.get_stmts_after,
                                   self.pkb.Next.get_stmts_before_t if with_star else self.pkb.Next.get_stmts_before,
                                   self.pkb.Next.is_next_t if with_star else self.pkb.Next.is_next)
        # Parent( p, q) gdzie p,q sa parametrami z pierwszej linii wejsciowej
        else:
            self.__execute_relation_double_unknown(items[0], items[1],
                                                   self.pkb.Next.get_stmts_after_t if with_star else self.pkb.Next.get_stmts_after,
                                                   self.pkb.Next.get_stmts_before_t if with_star else self.pkb.Next.get_stmts_before,
                                                   self.pkb.Next.is_next_t if with_star else self.pkb.Next.is_next)

    def __execute_follows_relation(self, items, with_star):
        arg1 = self.__format_argument(items[0])
        arg2 = self.__format_argument(items[1])
        available_types = (STMT, WHILE, ASSIGN, CALL, IF, PROG_LINE)

        # Follows(1,5)
        if items[0].arg_type == INTEGER and items[1].arg_type == INTEGER:
            is_connection = self.pkb.Follows.is_follows_t(arg1, arg2) if with_star else self.pkb.Follows.is_follows(
                arg1, arg2)
            if not is_connection:
                self.result_tree.set_no_results()

        # Follows(6, p)  lub Follows(6, _) gdzie p jest parametrem z pierwszej linii wejsciowej
        elif items[0].arg_type == INTEGER and (items[1].arg_type in available_types or items[1].arg_type == DEFAULT):
            values = self.pkb.Follows.get_followed_by_t(arg1) if with_star else self.pkb.Follows.get_followed_by(arg1)
            if values:
                if items[1].arg_type in available_types:
                    self.result_tree.modify_values(arg2, values)
            else:
                self.result_tree.set_no_results()

        # Follows( p, 1) lub Follows(_, 1) gdzie p jest parametrem z pierwszej linii wejsciowej
        elif (items[0].arg_type in available_types or items[0].arg_type == DEFAULT) and items[1].arg_type == INTEGER:
            values = self.pkb.Follows.get_followers_t(arg2) if with_star else self.pkb.Follows.get_follower(arg2)
            if values:
                if items[0].arg_type in available_types:
                    self.result_tree.modify_values(arg1, values)
            else:
                self.result_tree.set_no_results()
        # zmienna + '_'
        elif items[0].arg_type == DEFAULT or items[1].arg_type == DEFAULT:
            self.__execute_default(items[0], items[1],
                                   self.pkb.Follows.get_followed_by_t if with_star else self.pkb.Follows.get_followed_by,
                                   self.pkb.Follows.get_followers_t if with_star else self.pkb.Follows.get_follower,
                                   self.pkb.Follows.is_follows_t if with_star else self.pkb.Follows.is_follows)
        # Follows( p, q) gdzie p,q sa parametrami z pierwszej linii wejsciowej
        else:
            self.__execute_relation_double_unknown(items[0], items[1],
                                                   self.pkb.Follows.get_followed_by_t if with_star else self.pkb.Follows.get_followed_by,
                                                   self.pkb.Follows.get_followers_t if with_star else self.pkb.Follows.get_follower,
                                                   self.pkb.Follows.is_follows_t if with_star else self.pkb.Follows.is_follows)

    def __execute_parent_relation(self, items, with_star):
        arg1 = self.__format_argument(items[0])
        arg2 = self.__format_argument(items[1])

        available_types = (STMT, WHILE, ASSIGN, CALL, IF, PROG_LINE)
        # Parent(1,5)
        if items[0].arg_type == INTEGER and items[1].arg_type == INTEGER:
            is_connection = self.pkb.Parent.is_parent_t(arg1, arg2) if with_star else self.pkb.Parent.is_parent(
                arg1, arg2)
            if not is_connection:
                self.result_tree.set_no_results()

        # Parent(6, p) gdzie p jest parametrem z pierwszej linii wejsciowej
        elif items[0].arg_type == INTEGER and (items[1].arg_type in available_types or items[1].arg_type == DEFAULT):
            values = self.pkb.Parent.get_childs_t(arg1) if with_star else self.pkb.Parent.get_childs(arg1)
            if values:
                if items[1].arg_type in available_types:
                    self.result_tree.modify_values(arg2, values)
            else:
                self.result_tree.set_no_results()

        # Parent( p, "B") gdzie p jest parametrem z pierwszej linii wejsciowej
        elif (items[0].arg_type in available_types or items[0].arg_type == DEFAULT) and items[1].arg_type == INTEGER:
            values = self.pkb.Parent.get_parent_t(arg2) if with_star else self.pkb.Parent.get_parent(arg2)
            if values:
                if items[0].arg_type in available_types:
                    self.result_tree.modify_values(arg1, values)
            else:
                self.result_tree.set_no_results()
                # zmienna + '_'

        elif items[0].arg_type == DEFAULT or items[1].arg_type == DEFAULT:
            self.__execute_default(items[0], items[1],
                                   self.pkb.Parent.get_childs_t if with_star else self.pkb.Parent.get_childs,
                                   self.pkb.Parent.get_parent_t if with_star else self.pkb.Parent.get_parent,
                                   self.pkb.Parent.is_parent_t if with_star else self.pkb.Parent.is_parent)
        # Parent( p, q) gdzie p,q sa parametrami z pierwszej linii wejsciowej
        else:
            # print('dd')
            self.__execute_relation_double_unknown(items[0], items[1],
                                                   self.pkb.Parent.get_childs_t if with_star else self.pkb.Parent.get_childs,
                                                   self.pkb.Parent.get_parent_t if with_star else self.pkb.Parent.get_parent,
                                                   self.pkb.Parent.is_parent_t if with_star else self.pkb.Parent.is_parent)

    def __execute_with_extra_relation(self, items):

        value = items['value']
        value = int(value) if self.__is_integer(value) else value.replace('"', '')
        self.result_tree.modify_values(items['variable'], (value,))

    def __execute_modifies_relation(self, items):
        arg1 = self.__format_argument(items[0])
        arg2 = self.__format_argument(items[1])

        available_types = (STMT, WHILE, ASSIGN, CALL, IF, PROG_LINE)

        # procedure variable
        # Modifies("A", "c")
        if items[0].arg_type == STRING and items[1].arg_type == STRING:
            is_connection = self.pkb.Modifies.is_modifies_procedure(arg1, arg2)
            if not is_connection:
                self.result_tree.set_no_results()
                # Parent(6, p) gdzie p jest parametrem z pierwszej linii wejsciowej

        # procedure variable
        # Modifies("A", v)
        elif items[0].arg_type == STRING and items[1].arg_type in (VARIABLE, DEFAULT):
            values = self.pkb.Modifies.get_modifies_procedure(arg1)
            if values:
                if items[1].arg_type == VARIABLE:
                    self.result_tree.modify_values(arg2, values)
            else:
                self.result_tree.set_no_results()

        # procedure variable
        # Modifies(p, 's')
        elif items[0].arg_type == PROCEDURE and items[1].arg_type == STRING:
            values = self.pkb.Modifies.get_modified_in_procedure(arg2)
            if values:
                self.result_tree.modify_values(arg1, values)
            else:
                self.result_tree.set_no_results()

        # stmt, variable
        # Modifies(4, "v")
        elif items[0].arg_type == INTEGER and items[1].arg_type == STRING:
            is_connection = self.pkb.Modifies.is_modifies_stmt(arg1, arg2)
            if not is_connection:
                self.result_tree.set_no_results()
        # stmt variable
        # Modifies(10, v)
        elif items[0].arg_type == INTEGER and items[1].arg_type in (VARIABLE, DEFAULT):
            values = self.pkb.Modifies.get_modifies_stmt(arg1)
            if values:
                if items[1].arg_type == VARIABLE:
                    self.result_tree.modify_values(arg2, values)
            else:
                self.result_tree.set_no_results()

        # stmt variable
        # Modifies(s, 'a')
        elif items[0].arg_type in available_types and items[1].arg_type == STRING:
            values = self.pkb.Modifies.get_modified_in_stmt(arg2)
            if values:
                self.result_tree.modify_values(arg1, values)
            else:
                self.result_tree.set_no_results()
        # procedura + '_'
        elif items[0].arg_type == PROCEDURE and items[1].arg_type == DEFAULT:
            self.__execute_default(items[0], items[1],
                                   self.pkb.Modifies.get_modifies_procedure,
                                   self.pkb.Modifies.get_modified_in_procedure,
                                   self.pkb.Modifies.is_modifies_procedure)
        # stmt + '_'
        elif items[0].arg_type in available_types and items[1].arg_type == DEFAULT:
            self.__execute_default(items[0], items[1],
                                   self.pkb.Modifies.get_modifies_stmt,
                                   self.pkb.Modifies.get_modified_in_stmt,
                                   self.pkb.Modifies.is_modifies_stmt)
        # Modifies( p, q) gdzie p,q sa parametrami z pierwszej linii wejsciowej
        else:
            # procedury
            if items[0].arg_type == PROCEDURE:
                self.__execute_relation_double_unknown(items[0], items[1],
                                                       self.pkb.Modifies.get_modifies_procedure,
                                                       self.pkb.Modifies.get_modified_in_procedure,
                                                       self.pkb.Modifies.is_modifies_procedure)

            # stmt
            if items[0].arg_type in available_types:
                self.__execute_relation_double_unknown(items[0], items[1],
                                                       self.pkb.Modifies.get_modifies_stmt,
                                                       self.pkb.Modifies.get_modified_in_stmt,
                                                       self.pkb.Modifies.is_modifies_stmt)

    def __execute_uses_relation(self, items):
        arg1 = self.__format_argument(items[0])
        arg2 = self.__format_argument(items[1])

        available_types = (STMT, WHILE, ASSIGN, CALL, IF, PROG_LINE)

        # procedure variable
        # Uses("A", "c")
        if items[0].arg_type == STRING and items[1].arg_type == STRING:
            is_connection = self.pkb.Uses.is_uses_procedure(arg1, arg2)
            if not is_connection:
                self.result_tree.set_no_results()
                # Parent(6, p) gdzie p jest parametrem z pierwszej linii wejsciowej

        # procedure variable
        # Uses("A", v)
        elif items[0].arg_type == STRING and items[1].arg_type in (VARIABLE, DEFAULT):
            values = self.pkb.Uses.get_uses_procedure(arg1)
            if values:
                if items[1].arg_type == VARIABLE:
                    self.result_tree.modify_values(arg2, values)
            else:
                self.result_tree.set_no_results()

        # procedure variable
        # Uses(p, 's')
        elif items[0].arg_type == PROCEDURE and items[1].arg_type == STRING:
            values = self.pkb.Uses.get_used_in_procedure(arg2)
            if values:
                self.result_tree.modify_values(arg1, values)
            else:
                self.result_tree.set_no_results()

        # stmt, variable
        # Uses(4, "v")
        elif items[0].arg_type == INTEGER and items[1].arg_type == STRING:
            is_connection = self.pkb.Uses.is_uses_stmt(arg1, arg2)
            if not is_connection:
                self.result_tree.set_no_results()
        # stmt variable
        # Uses(10, v)
        elif items[0].arg_type == INTEGER and items[1].arg_type in (VARIABLE, DEFAULT):
            values = self.pkb.Uses.get_uses_stmt(arg1)
            if values:
                if items[1].arg_type == VARIABLE:
                    self.result_tree.modify_values(arg2, values)
            else:
                self.result_tree.set_no_results()

        # stmt variable
        # Uses(s, 'a')
        elif items[0].arg_type in available_types and items[1].arg_type == STRING:
            values = self.pkb.Uses.get_used_in_stmt(arg2)
            if values:
                self.result_tree.modify_values(arg1, values)
            else:
                self.result_tree.set_no_results()
        # procedura + '_'
        elif items[0].arg_type == PROCEDURE and items[1].arg_type == DEFAULT:
            self.__execute_default(items[0], items[1],
                                   self.pkb.Uses.get_uses_procedure,
                                   self.pkb.Uses.get_used_in_procedure,
                                   self.pkb.Uses.is_uses_procedure)
            # stmt + '_'
        elif items[0].arg_type in available_types and items[1].arg_type == DEFAULT:
            self.__execute_default(items[0], items[1],
                                   self.pkb.Uses.get_uses_stmt,
                                   self.pkb.Uses.get_used_in_stmt,
                                   self.pkb.Uses.is_uses_stmt)
        # Uses( p, q) gdzie p,q sa parametrami z pierwszej linii wejsciowej
        else:
            # procedury
            if items[0].arg_type == PROCEDURE:
                self.__execute_relation_double_unknown(items[0], items[1],
                                                       self.pkb.Uses.get_uses_procedure,
                                                       self.pkb.Uses.get_used_in_procedure,
                                                       self.pkb.Uses.is_uses_procedure)

            # stmt
            if items[0].arg_type in available_types:
                self.__execute_relation_double_unknown(items[0], items[1],
                                                       self.pkb.Uses.get_uses_stmt,
                                                       self.pkb.Uses.get_used_in_stmt,
                                                       self.pkb.Uses.is_uses_stmt)

# @TODO ogarnac exceptions do wyswietlania w pipetester - JAKIS TAM WSTEP JUZ JEST

# @TODO WITH , PATTERN
