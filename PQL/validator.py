import re
import PQL.keywords as kw
from PQL.keywords import NEXT_KEYWORDS, GRAMMAR_KEYWORDS, QUERY_KEYWORDS, RELATION_KEYWORDS

RE_PARAM_0 = '\([a-zA-Z0-9_]+,[a-zA-Z0-9_]+\)'
RE_PARAM_1 = '\(\"[a-zA-Z0-9_]+\",[a-zA-Z0-9_]+\)'
RE_PARAM_2 = '\([a-zA-Z0-9_]+,\"[a-zA-Z0-9_]+\"\)'
RE_PARAM_3 = '\(\"[a-zA-Z0-9_]+\",\"[a-zA-Z0-9_]+\"\)'

RE_PARENT = kw.PARENT + RE_PARAM_0 + '|' + kw.PARENT + RE_PARAM_1 + "|" + kw.PARENT + RE_PARAM_2 + "|" + kw.PARENT + RE_PARAM_3
RE_PARENT_S = 'Parent\*' + RE_PARAM_0 + '|' + 'Parent\*' + RE_PARAM_1 + "|" + 'Parent\*' + RE_PARAM_2 + "|" + 'Parent\*' + RE_PARAM_3
RE_USES = kw.USES + RE_PARAM_0 + '|' + kw.USES + RE_PARAM_1 + "|" + kw.USES + RE_PARAM_2 + "|" + kw.USES + RE_PARAM_3
RE_MODIFIES = kw.MODIFIES + RE_PARAM_0 + '|' + kw.MODIFIES + RE_PARAM_1 + "|" + kw.MODIFIES + RE_PARAM_2 + "|" + kw.MODIFIES + RE_PARAM_3
RE_FOLLOWS = kw.FOLLOWS + RE_PARAM_0 + '|' + kw.FOLLOWS + RE_PARAM_1 + "|" + kw.FOLLOWS + RE_PARAM_2 + "|" + kw.FOLLOWS + RE_PARAM_3
RE_FOLLOWS_S = 'Follows\*' + RE_PARAM_0 + '|' + 'Follows\*' + RE_PARAM_1 + "|" + 'Follows\*' + RE_PARAM_2 + "|" + 'Follows\*' + RE_PARAM_3
RE_NEXT = kw.NEXT + RE_PARAM_0 + '|' + kw.NEXT + RE_PARAM_1 + "|" + kw.NEXT + RE_PARAM_2 + "|" + kw.NEXT + RE_PARAM_3
RE_NEXT_S = 'Next\*' + RE_PARAM_0 + '|' + 'Next\*' + RE_PARAM_1 + "|" + 'Next\*' + RE_PARAM_2 + "|" + 'Next\*' + RE_PARAM_3
RE_CALLS = kw.CALLS + RE_PARAM_0 + '|' + kw.CALLS + RE_PARAM_1 + "|" + kw.CALLS + RE_PARAM_2 + "|" + kw.CALLS + RE_PARAM_3
RE_CALLS_S = 'Calls\*' + RE_PARAM_0 + '|' + 'Calls\*' + RE_PARAM_1 + "|" + 'Calls\*' + RE_PARAM_2 + "|" + 'Calls\*' + RE_PARAM_3

REGEX_RELATIONS = \
    RE_PARENT + '|' + RE_PARENT_S + '|' + RE_USES + '|' + RE_MODIFIES + '|' + RE_FOLLOWS \
    + '|' + RE_FOLLOWS_S + '|' + RE_NEXT + '|' + RE_NEXT_S + '|' + RE_CALLS + '|' + RE_CALLS_S


def is_relation(item):
    return re.match(r"{}".format(REGEX_RELATIONS), item) is not None


#
# def is_relation_keyword(item):
#     return item in RELATION_KEYWORDS


def is_valid_next_keyword(item, item2):
    return item2 in NEXT_KEYWORDS[item]


# do pierwszej linii w query
def is_param_a_keyword(item):
    return item in GRAMMAR_KEYWORDS


def is_param_name(item):
    return item not in GRAMMAR_KEYWORDS + RELATION_KEYWORDS + QUERY_KEYWORDS
