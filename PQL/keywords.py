SELECT = 'Select'
SUCH_THAT = 'such that'
WITH = 'with'
PATTERN = 'pattern'
AND = 'and'

# Grammar keywords
PROCEDURE = 'procedure'
ASSIGN = 'assign'
STMT = 'stmt'
WHILE = 'while'
IF = 'if'
PROG_LINE = 'prog_line'
VARIABLE = 'variable'
CONSTANT = 'constant'
CALL = 'call'

# Relation keywords
PARENT = 'Parent'
PARENT_S = 'Parent*'
USES = 'Uses'
MODIFIES = 'Modifies'
FOLLOWS = 'Follows'
FOLLOWS_S = 'Follows*'
NEXT = 'Next'
NEXT_S = 'Next*'
CALLS = 'Calls'
CALLS_S = 'Calls*'

# With keywords
PROC_NAME = 'procName'
VAR_NAME = 'varName'
STMT_NUMBER = 'stmt#'
VALUE = 'value'

# Pattern expression types
EXPR = 'expr'
SUB_EXPR = 'sub_expr'
FLOOR = '_'

# args types
INTEGER = 'integer'
STRING = 'string'
DEFAULT = 'default'

RELATION_KEYWORDS = [
    PARENT,
    PARENT_S,
    USES,
    MODIFIES,
    FOLLOWS,
    FOLLOWS_S,
    NEXT,
    NEXT_S,
    CALLS,
    CALLS_S
]

GRAMMAR_KEYWORDS = [
    PROCEDURE,
    ASSIGN,
    STMT,
    WHILE,
    IF,
    PROG_LINE,
    VARIABLE,
    CONSTANT,
    CALL
]

QUERY_KEYWORDS = [
    SELECT,
    SUCH_THAT,
    WITH,
    PATTERN,
    AND,
]

QUERY_KEYWORDS_MULTIPLE = [
    SUCH_THAT,
    WITH,
    PATTERN,
    AND,
]

STMT_TYPES = [
    STMT,
    ASSIGN,
    CALL,
    WHILE,
    IF
]

WITH_ATTRIBUTES_KEYWORDS = [
    PROC_NAME,
    VAR_NAME,
    STMT_NUMBER,
    VALUE,
    PROG_LINE
]
PATTERN_PARAMS = [
    ASSIGN,
    WHILE,
    IF
]

PATTERN_EXPR_TYPES = [
    EXPR,
    SUB_EXPR,
    FLOOR
]
# Słownik ktory pokazuje jakie moga byc kolejne instrukcje po danym slowie skluczowym
NEXT_KEYWORDS = {
    # przy select pamietajmy ze to nie jest bezposrednio kolejne, bo po select musi byc zmienna
    SELECT: [SUCH_THAT, PATTERN, WITH],
    SUCH_THAT: RELATION_KEYWORDS,
    # @TODO pozniej uzupelnic
    AND: [RELATION_KEYWORDS],
    PATTERN: [],
    WITH: []
}

BOOLEAN = 'BOOLEAN'


class ERRORS:
    INVALID_PARAM_LENGTH = "Invalid params - not enough details."
    INVALID_PARAM_WRONG_DETAILS = "Invalid params - invalid params definition."

    INVALID_QUERY_SELECT = "Invalid query - the first word is not 'Select'."
    INVALID_QUERY_WRONG_PARAM_NAME = "Invalid query - the second word is not parameter name."
    INVALID_QUERY_WRONG_KEYWORD = "Invalid query - invalid query keyword."
    INVALID_QUERY_WRONG_RELATION_WORD = "Invalid query - invalid relation keyword."
    INVALID_QUERY_WRONG_RELATION_ARGS = "Invalid query - invalid relation arguments."
    UNDECLARED_RELATION_PARAM = "Undeclared relation parameter."
